/**
 * form validate
 */
$(document).on('input', 'input.input-number', function(){
    this.value = this.value.replace(/[^0-9]/g, '');
    this.value = this.value.replace(/(\..*)\./g, '$1');
});

$(document).ready(function() {    
    PageDetailCollapse();
    PageDetailCollapseV2();
    CheckOnResizeFAQ();
    $('.page-detail-card .remark-content').slideUp();
});
window.onresize = function(event) {
    PageDetailCollapse();
    PageDetailCollapseV2();
    CheckOnResizeFAQ();

    CheckOnShowItemDigital();
    CheckOnResizeDigital();
    CheckCollapseDigitalBankingPromotion();
};

$(document).on('click', '.page-detail-card .btn-collapse', function(){

    let foo = this;
    $(this).prop('disabled', true);

    if ($(this).hasClass('active')) {
        $('.page-detail-card .btn-collapse').removeClass('active');

        $(this).find('img.collapse-arrow').attr('hidden', false);
        $(this).find('img.collapse-arrow-active').attr('hidden', true);
    }
    else {
        $('.page-detail-card .btn-collapse').removeClass('active');
        $('.page-detail-card .btn-collapse').find('img.collapse-arrow').attr('hidden', false);
        $('.page-detail-card .btn-collapse').find('img.collapse-arrow-active').attr('hidden', true);

        $(this).addClass('active');
        $(this).find('img.collapse-arrow').attr('hidden', true);
        $(this).find('img.collapse-arrow-active').attr('hidden', false);
    }

    setTimeout(function() {
        $(foo).prop('disabled', false);
    }, 300);
});


function PageDetailCollapse() {
    if (window.innerWidth >= 768) {
        $('.page-detail-accordion div.collapse:not(.collapse-remark)').addClass('show');
    } 
    else {
        $('.page-detail-accordion div.collapse:not(.collapse-remark)').removeClass('show');        
        $('.page-detail-accordion .btn-collapse').removeClass('active');

        $('.page-detail-accordion button').first().click();
    }
}

// ใช้หน้าดอกเบี่้ยเงินฝากย้อนหลัง
function PageDetailCollapseV2() {
    
    if (window.innerWidth <= 768) {
        $('.page-detail-accordion-v2 .page-detail-card-v2').show();
        $('.page-detail-accordion-v2 .btn-collapse').removeClass('active');
        $('.page-detail-accordion-v2 div.collapse:not(.collapse-remark)').removeClass('show');
        $('.page-detail-accordion-v2 div.collapse:not(.collapse-remark)').removeAttr('style');

        $('.page-detail-accordion-v2 button').first().click();
    } else {
        $('.page-detail-accordion-v2 div.collapse:not(.collapse-remark)').removeClass('show');
        $('.page-detail .a-collapse-v2').first().click();
    }
}
$('.page-detail a.a-anchor').click(function(){
    $('.page-detail a.a-anchor').removeClass('active');
    $(this).addClass('active');
});
$(document).on('click', '.page-detail .a-collapse-v2', function(){

    let collapse = $(this).data('collapse');
    let section = $(this).data('section');
    
    if (!$(this).hasClass('active')) {
        $('.page-detail .a-collapse-v2').removeClass('active');
        $(this).addClass('active');

        $(`.page-detail .page-detail-card-v2`).hide();
        $(`.page-detail .page-detail-accordion-v2 #${section}`).show();
        
        $(`.page-detail .page-detail-accordion-v2 div.collapse`).hide();
        $(`.page-detail .page-detail-accordion-v2 div#${collapse}`).slideDown();
    }
});
$(document).on('click', '.page-detail-card-v2 .btn-collapse', function(){

    let foo = this;
    $(this).prop('disabled', true);

    if ($(this).hasClass('active')) {
        $('.page-detail-card-v2 .btn-collapse').removeClass('active');

        $(this).find('img.collapse-arrow').attr('hidden', false);
        $(this).find('img.collapse-arrow-active').attr('hidden', true);
    }
    else {
        $('.page-detail-card-v2 .btn-collapse').removeClass('active');
        $('.page-detail-card-v2 .btn-collapse').find('img.collapse-arrow').attr('hidden', false);
        $('.page-detail-card-v2 .btn-collapse').find('img.collapse-arrow-active').attr('hidden', true);

        $(this).addClass('active');
        $(this).find('img.collapse-arrow').attr('hidden', true);
        $(this).find('img.collapse-arrow-active').attr('hidden', false);
    }

    setTimeout(function() {
        $(foo).prop('disabled', false);
    }, 300);
});

$(document).on('click', '.page-detail-card .remark-title', function(){
    
    $('.page-detail-card .remark-content').slideUp();

    if ($(this).hasClass('active')) {
        $('.page-detail-card .remark-title').removeClass('active');

        $(this).find('img.collapse-arrow').attr('hidden', false);
        $(this).find('img.collapse-arrow-active').attr('hidden', true);
    }
    else {
        $('.page-detail-card .remark-title').removeClass('active');
        $('.page-detail-card .remark-title').find('img.collapse-arrow').attr('hidden', false);
        $('.page-detail-card .remark-title').find('img.collapse-arrow-active').attr('hidden', true);

        $(this).addClass('active');
        $(this).find('img.collapse-arrow').attr('hidden', true);
        $(this).find('img.collapse-arrow-active').attr('hidden', false);

        let dataRemark = $(this).data('remark');
        $(`.remark-content[data-remark='${dataRemark}']`).slideDown();
    }
});

document.querySelectorAll('a.a-anchor').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});


/**
 * เมนูที่เกี่ยวข้องอื่น ๆ
 */
$(document).ready(function(){

    var owl = $('.page-detail .owl-carousel');
    
    owl.owlCarousel({
        center: false,
        margin: 10,
        padding: 0,
        loop:true,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            900:{
                items:3
            }
        }
    });

    $('.section-relate-other .btn-card-other-prev').click(function() {
        owl.trigger('prev.owl.carousel');
    })

    $('.section-relate-other .btn-card-other-next').click(function() {
        owl.trigger('next.owl.carousel', [300]);
    })

    $('.page-detail.section-relate-other .card-title').click(function(){
        alert('goto-link');
    });
});


$(document).on('click', '.menu-collapse .btn-menu-collapse', function(){

    let target = $(this).data('target');
    let foo = this;
    $(this).prop('disabled', true);

    if ($(this).hasClass('active')) {
        
        $(this).closest('.menu-collapse').find('.btn-menu-collapse').removeClass('active');
        $(this).closest('.menu-collapse').find('.menu-collapse-sub').slideUp(300);

        $(this).find('img.collapse-arrow').attr('hidden', false);
        $(this).find('img.collapse-arrow-active').attr('hidden', true);
    }
    else {

        $(this).closest('.menu-collapse').find('.btn-menu-collapse img.collapse-arrow').attr('hidden', false);
        $(this).closest('.menu-collapse').find('.btn-menu-collapse img.collapse-arrow-active').attr('hidden', true);

        $(this).closest('.menu-collapse').find('.btn-menu-collapse').removeClass('active');
        $(this).closest('.menu-collapse').find('.menu-collapse-sub').slideUp(300);

        $(this).addClass('active');
        $(this).find('img.collapse-arrow').attr('hidden', true);
        $(this).find('img.collapse-arrow-active').attr('hidden', false);

        $(`#${target}`).slideDown(300);
    }

    setTimeout(function() {
        $(foo).prop('disabled', false);
    }, 300);
});


$(".section-menu-product .menu-group-deposit .navbar .nav-link").click(function () {
    $(".section-menu-product .menu-group-deposit .navbar .nav-link").removeClass("active");
    $(this).addClass("active");
    var x = window.matchMedia("(max-width: 767px)")
    if (x.matches) {
        $(".section-menu-product .menu-group-deposit button.navbar-toggler").click();
    }
    $(".section-product ").removeClass("active");
    var tag = "#deposit" + $(this).data("value");
    var targetid = $(tag);
    targetid.addClass("active");
});

/**
 * เปิดเมนูเครื่องคำนวน
 */
$(document).on('click', '.page-detail .btn-calculator-mobile-menu', function(){
    $('.modal-calculator-menu').modal('show');
});
$(document).on('click', '.page-detail .btn-droplead', function(){
    $('.modal-droplead').modal('show');
});


/**
 * Droplead input error
 */
$(document).on('click', '.modal-droplead .droplead-submit', function(e){
    e.preventDefault();
    let error = false;

    $('.modal-droplead form input[required]').removeClass('error');
    $('.modal-droplead form span.droplead-error').attr('hidden', true);
    $('.modal-droplead form input[required]').each(function(){        
        if (this.value.trim() == "") {
            $(this).addClass('error');
            $(this).next('span').attr('hidden', false);
            error = true;
        }
    });

    if (error != true) {
        // submit
    } else {
        return false;
    }    
});



/**
  FAQ DETAIL TYPE B
 **/

  $(".page-detail-faqB.page-detail-faq .nav-anchor .nav-item .nav-link").click(function () {
    $(".page-detail-faqB.page-detail-faq .nav-anchor .nav-item .nav-link").removeClass("active");
    $(this).addClass("active");
});

let count_item_FAQ_Main = 0;
let count_item_FAQ_Main_SME = 0;
let show_item_FAQ_Main = 6;
let show_item_FAQ_Main_SME = 6;
$(document).ready(function () {
    count_item_FAQ_Main = $(".section-product-faq.section-product#deposit1 .products-cards .center-mb").length;
    count_item_FAQ_Main_SME = $(".section-product-faq.section-product#deposit2 .products-cards .center-mb").length;
    if (count_item_FAQ_Main <= 6) {
            $(".section-product-faq.section-product#deposit1 .products-cards .page-detail").addClass("d-none");
            $(".section-product-faq.section-product#deposit1 .products-cards .page-detail").removeClass("d-block");
        }

    if (count_item_FAQ_Main_SME <= 6) {
            $(".section-product-faq.section-product#deposit2 .products-cards .page-detail").addClass("d-none");
            $(".section-product-faq.section-product#deposit2 .products-cards .page-detail").removeClass("d-block");
        }

    CheckOnResizeFAQ();
    });

function CheckOnResizeFAQ() {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    if (x.matches) {
        $('.section-product-faq.section-product#deposit1 .products-cards .center-mb').each(function () {
            begin++;
            if ((begin > show_item_FAQ_Main) && (count_item_FAQ_Main >= begin)) {
                $(this).addClass("d-none");
            }
            else {
                $(this).removeClass("d-none");
            }
        });

        begin = 0;
        $('.section-product-faq.section-product#deposit2 .products-cards .center-mb').each(function () {
            begin++;
            if ((begin > show_item_FAQ_Main_SME) && (count_item_FAQ_Main_SME >= begin)) {
                $(this).addClass("d-none");
            }
            else {
                $(this).removeClass("d-none");
            }
        });
    }
    else {
        show_item_FAQ_Main = 6;
        show_item_FAQ_Main_SME = 6;
        begin = 0;
        $('.section-product-faq.section-product#deposit1 .products-cards .center-mb').each(function () {
            begin++;
            if (count_item_FAQ_Main >= begin) {
                $(this).removeClass("d-none");
            }
        });

        begin = 0;
        $('.section-product-faq.section-product#deposit2 .products-cards .center-mb').each(function () {
            begin++;
            if (count_item_FAQ_Main_SME >= begin) {
                $(this).removeClass("d-none");
            }
        });
    }
}

function FAQMore(tab) {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    if (x.matches) {

        if (tab == 'A') {

            begin = 0;
            show_item_FAQ_Main += 3;
            $('.section-product-faq.section-product#deposit1 .products-cards .center-mb').each(function () {
                begin++;

                if ((begin <= show_item_FAQ_Main) && (count_item_FAQ_Main >= begin)) {
                    $(this).removeClass("d-none");
                }
            });

            if ($(".section-product-faq.section-product#deposit1 .products-cards .center-mb.d-none").length ==0) {
                $(".section-product-faq.section-product#deposit1 .products-cards .page-detail.center-mb").addClass("d-none");
                $(".section-product-faq.section-product#deposit1 .products-cards .page-detail.center-mb").removeClass("d-block");
            }
        }
        else if (tab == 'B') {
            begin = 0;
            show_item_FAQ_Main_SME += 3;
            $('.section-product-faq.section-product#deposit2 .products-cards .center-mb').each(function () {
                begin++;
                if ((begin <= show_item_FAQ_Main_SME) && (count_item_FAQ_Main_SME >= begin)) {
                    $(this).removeClass("d-none");
                }
            });

            if ($(".section-product-faq.section-product#deposit2 .products-cards .center-mb.d-none").length ==0) {
                $(".section-product-faq.section-product#deposit2 .products-cards .page-detail.center-mb").addClass("d-none");
                $(".section-product-faq.section-product#deposit2 .products-cards .page-detail.center-mb").removeClass("d-block");
            }
        }
    }
}


$(document).ready(function(){

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if (scroll <= 680) {
            // $('.nav-anchor ul li a').removeClass('active');
            // $('.nav-anchor ul li a').first().addClass('active');
        }
    });

    
});


/**
  loan form other
 **/
$(".page-form-other .page-detail-description .page-form-other-link a").click(function () {
    $(".page-form-other .page-detail-description .page-form-other-link a").removeClass("active");
    $(this).addClass("active");
});

/**
  start calculator loan all lot04
 **/
$(".page-cal-loan.page-detail .calculator-menu .menu-collapse a").click(function () {
    var value = $(this).data("value");
    //console.log("value:" + value)
    if (value == undefined) return;

    $(".page-cal-loan.page-detail .calculator-menu .menu-collapse a").removeClass("active");
    $(".page-cal-loan.page-detail .calculator-menu .menu-collapse a").each(function () {
        var data_value = $(this).data("value");
        if (data_value == value)
            $(this).addClass("active");
    });
    var main_sub = $(this).data("main-sub");
    if (value != undefined) {
        var tag = ".page-cal-loan.page-detail .calculator-menu .menu-collapse a." + main_sub;
        $(tag).addClass("active");
    }
    $(".page-cal-loan.page-detail div.calculator-content").addClass("d-none");
    var tagid = "#" + value;
    $(tagid).removeClass("d-none");
});

let count_item_digital = 0;
let show_item_digital_PC = 5;
let show_item_digital_MB = 6;
$(document).ready(function () {
    count_item_digital = $(".page-digital-banking-M-choice.section-product .products-cards .center-mb").length;

    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    CheckOnShowItemDigital();
    CheckOnResizeDigital();
});

function  getClassNameitem(total,counting) {
    let nameItem = '';
    if (total <=5) {
        nameItem = 'item' + total + '-' + counting;
    }
    else {
        nameItem = 'itemN-' + counting;
    }
    return nameItem;
}

function CheckOnResizeDigital() {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    if (!x.matches) {
        $('.page-digital-banking-M-choice.section-product .products-cards .grid-cards-container-mb').addClass('grid-cards-container');
        $('.page-digital-banking-M-choice.section-product .products-cards .grid-cards-container').removeClass('grid-cards-container-mb');
      
            $('.page-digital-banking-M-choice.section-product .products-cards .center-mb').each(function () {
                begin++;
                let nameItem = getClassNameitem(count_item_digital, begin);
                if (((begin == 1)&&(count_item_digital != 3)) || (count_item_digital==2)) {
                    //promotion - type - 1
                    $(this).addClass('grid-item');
                    $(this).addClass(nameItem);
                    $(this.firstElementChild).addClass('promotion-type-1');
                    $(this).find(".item .item-container .card-img img").css("display", "none");
                    let imgCurrent = $(this).find(".item .item-container .card-img img").attr('src');
                    $(this).find(".item .item-img-background").css('background-image', 'url(' + imgCurrent + ')');
                }
                else if ((begin == 5)||((begin == 1) && (count_item_digital == 3))) {
                    //promotion - type - 3
                    $(this).addClass('grid-item');
                    $(this).addClass(nameItem);

                    var windowMediaX = window.matchMedia("(max-width: 950px)")
                    if (!windowMediaX.matches) {
                        $(this.firstElementChild).removeClass('promotion-type-1');
                        $(this).find(".item .item-container .card-img img").css("display", "unset");
                        let imgCurrent = $(this).find(".item .item-container .card-img img").attr('src');
                        $(this).find(".item .item-img-background").css('background-image', 'none');
                        $(this.firstElementChild).addClass('promotion-type-3');
                    }
                    else {
                        $(this.firstElementChild).removeClass('promotion-type-3');
                        $(this.firstElementChild).addClass('promotion-type-1');
                        $(this).find(".item .item-container .card-img img").css("display", "none");
                        let imgCurrent = $(this).find(".item .item-container .card-img img").attr('src');
                        $(this).find(".item .item-img-background").css('background-image', 'url(' + imgCurrent + ')');
                    }
                }
                else {
                    //promotion - type - 2
                    $(this).addClass('grid-item');
                    $(this).addClass(nameItem);
                    $(this.firstElementChild).addClass('promotion-type-2');
                }
            });
    }
    else {
        $('.page-digital-banking-M-choice.section-product .products-cards .grid-cards-container').addClass('grid-cards-container-mb');
        $('.page-digital-banking-M-choice.section-product .products-cards .grid-cards-container-mb').removeClass('grid-cards-container');
       
        $('.page-digital-banking-M-choice.section-product .products-cards .center-mb').each(function () {
            begin++;
            let nameItem = getClassNameitem(count_item_digital, begin);

            if (((begin == 1) && (count_item_digital != 3)) || (count_item_digital == 2)) {
                //promotion - type - 1
                $(this).removeClass('grid-item');
                $(this).removeClass(nameItem);
                $(this.firstElementChild).removeClass('promotion-type-1');
                $(this).find(".item .item-container .card-img img").css("display", "unset");
                let imgCurrent = $(this).find(".item .item-container .card-img img").attr('src');
                $(this).find(".item .item-img-background").css('background-image', 'none');
            }
            else if ((begin == 5) || ((begin == 1) && (count_item_digital == 3))) {
                //promotion - type - 3
                $(this).removeClass('grid-item');
                $(this).removeClass(nameItem);
                $(this.firstElementChild).removeClass('promotion-type-3');
                $(this.firstElementChild).removeClass('promotion-type-1');
                $(this).find(".item .item-container .card-img img").css("display", "unset");
                let imgCurrent = $(this).find(".item .item-container .card-img img").attr('src');
                $(this).find(".item .item-img-background").css('background-image', 'none');
            }
            else {
                //promotion - type - 2
                $(this).removeClass('grid-item');
                $(this).removeClass(nameItem);
                $(this.firstElementChild).removeClass('promotion-type-2');
            }
        });
    }
}

function CheckOnShowItemDigital() {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;

    if (!x.matches) {
        if (count_item_digital <= show_item_digital_PC) {
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").addClass("d-none");
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").removeClass("d-block");
        }
    }
    else {
        if (count_item_digital <= show_item_digital_MB) {
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").addClass("d-none");
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").removeClass("d-block");
        }
    }


    if (x.matches) {
        $('.page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb').each(function () {
            begin++;
            if ((begin > show_item_digital_MB) && (count_item_digital >= begin)) {
                $(this).addClass("d-none");
            }
            else {
                $(this).removeClass("d-none");
            }
        });
    }
    else {
        begin = 0;
        $('.page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb').each(function () {
            begin++;
            if ((begin > show_item_digital_PC) && (count_item_digital >= begin)) {
                $(this).addClass("d-none");
            }
            else {
                $(this).removeClass("d-none");
            }
        });
      
    }
}

function DigitalBankingPromotionMore() {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    if (x.matches) {

            begin = 0;
        show_item_digital_MB += 3;
        $('.page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb').each(function () {
                begin++;

                if ((begin <= show_item_digital_MB) && (count_item_digital >= begin)) {
                    $(this).removeClass("d-none");
                }
            });

        if ($(".page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb.d-none").length == 0) {
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").addClass("d-none");
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").removeClass("d-block");
            }
       
    }
    else {
        begin = 0;
        show_item_digital_PC += 6;
        $('.page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb').each(function () {
            begin++;

            if ((begin <= show_item_digital_PC) && (count_item_digital >= begin)) {
                $(this).removeClass("d-none");
            }
        });

        if ($(".page-digital-banking-M-choice.section-product .products-cards-item .products-cards .center-mb.d-none").length == 0) {
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").addClass("d-none");
            $(".page-digital-banking-M-choice.section-product .products-cards-item .page-detail").removeClass("d-block");
        }

    }
}


$(".page-digital-banking-promotion-detail.page-detail .nav .nav-link").click(function () {
    $(".page-digital-banking-promotion-detail .page-detail .nav .nav-link").removeClass("active");
    $(this).addClass("active");
});

$(".page-digital-banking-promotion-detail.page-detail .card-body .digital-promotion-menu-title").click(function () {
    var x = window.matchMedia("(max-width: 767px)")
    var begin = 0;
    if (!x.matches) {
        var pageDetatil = $(this.parentElement).find(".page-detail-description");
        if (pageDetatil.hasClass("d-md-none")) {
            $(this.parentElement).find(".page-detail-description").removeClass('d-md-none');
            $(this).removeClass('active');
            $(this).find(".collapse-arrow-active").css('margin', 'auto');
            $(this).find(".collapse-arrow-active").attr('hidden', false);
            $(this).find(".collapse-arrow").attr('hidden', true);
            $(this).find(".collapse-arrow").css('margin', 'unset');
        }
        else {
            $(this.parentElement).find(".page-detail-description").addClass('d-md-none');
            $(this).addClass('active');
            $(this).find(".collapse-arrow-active").css('margin','unset');
            $(this).find(".collapse-arrow-active").attr('hidden', true);
            $(this).find(".collapse-arrow").css('margin', 'auto');
            $(this).find(".collapse-arrow").attr('hidden', false);
        }
    }
});

function CheckCollapseDigitalBankingPromotion() {
    var x = window.matchMedia("(max-width: 767px)")
    if (!x.matches) {
        $(".page-digital-banking-promotion-detail.page-detail .card.page-detail-card .collapse").each(function () {
            if (!$(this).hasClass('show')) {
                console.log($(this).hasClass('show'))
                $(this).addClass('show')
            }
        });
    }
    else {
        $(".page-digital-banking-promotion-detail.page-detail .card.page-detail-card .collapse").removeClass('show')
        var begin = 0;
        $(".page-digital-banking-promotion-detail.page-detail .card.page-detail-card .collapse").each(function () {
            if (begin == 0) {
                $(this).addClass('show');
            }
            begin++;
        }
        );
    }
}

$(".section-1-product .life-stage-product ul li.menudeposit").click(function () {
    $(".section-1-product .life-stage-product ul li.menudeposit").removeClass("active");
    $(this).addClass("active");
});





// Collapse แบบหลายอันได้ในหน้าเดียว
$(document).on('click', '.div-button-collapse-multiselect', function(){
    let target = $(this).data('target');
    if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(target).slideUp();
    } else {
        $(this).addClass('open');
        $(target).slideDown();
    }
});