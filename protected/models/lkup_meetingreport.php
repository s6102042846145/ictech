<?php
class lkup_meetingreport extends CActiveRecord

{

    public static function getSearch()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,file_name,name,CONCAT(DATE_FORMAT( file_date , '%d' ),'/', DATE_FORMAT( file_date , '%m' ) ,'/',DATE_FORMAT( file_date , '%Y' ) +543) file_date,file_path from ictech_tran_meetingreport where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    
    public static function getUserSearchHead()
	{
        $createby = Yii::app()->user->getInfo('create_by');	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,name,YEAR(file_date) years from ictech_tran_meetingreport where status!=0 ".$sqlCon;	
        $sql.=" GROUP BY name order by name desc";
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getUserSearch($id = null)
	{
        $sql="select id,file_name,name,file_date,file_path from ictech_tran_meetingreport where status!=0 and YEAR(file_date)='".$id."' ";
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getData($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select id
,name
,file_path
,file_name
,CONCAT(DATE_FORMAT( file_date , '%d' ),'/', DATE_FORMAT( file_date , '%m' ) ,'/',DATE_FORMAT( file_date , '%Y' ) +543) file_date from ictech_tran_meetingreport where id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	
}	

