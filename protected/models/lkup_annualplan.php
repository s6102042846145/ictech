<?php
class lkup_annualplan extends CActiveRecord

{

    public static function getSearch()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,file_name,name,CONCAT(DATE_FORMAT( file_date , '%d' ),'/', DATE_FORMAT( file_date , '%m' ) ,'/',DATE_FORMAT( file_date , '%Y' ) +543) file_date,file_path from ictech_tran_annualplan where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getUserSearch()
	{
        $createby = Yii::app()->user->getInfo('create_by');	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,file_name,name,file_date,file_path from ictech_tran_annualplan where status!=0 ".$sqlCon;	
        $sql.=" order by name desc";
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    public static function getData($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select id
,name
,file_path
,file_name
,CONCAT(DATE_FORMAT( file_date , '%d' ),'/', DATE_FORMAT( file_date , '%m' ) ,'/',DATE_FORMAT( file_date , '%Y' ) +543) file_date from ictech_tran_annualplan where id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	
}	

