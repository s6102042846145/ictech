<?php


class lkup_user extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'mas_user';
	}

    public function attributeLabels() {
        return array(
        );
    }
	public function search($keyword=null,$lavel=null) 
	{
		
		$sqlCon="";
		if(Yii::app()->user->getInfo('lavel')!=1){			
			$sqlCon.=" and a.create_by=".Yii::app()->user->getInfo('id');
		}
		
		if($keyword!=''){
			$sqlCon.= " and (a.code like '%".$keyword."%' ";
			$sqlCon.= " or a.name  like '%".$keyword."%') ";	
			/*		
			$sqlCon.= " or line  like '%".$keyword."%' ";	
			$sqlCon.= " or facebook  like '%".$keyword."%' ";	
			$sqlCon.= " or address  like '%".$keyword."%' ";	
			$sqlCon.= " or tel like '%".$keyword."%') ";	
			*/
			
		}
		if($lavel!=''){
			$sqlCon.= " and a.lavel ='".$lavel."' ";
		}
		
		$count=Yii::app()->db->createCommand("select count(*) from mas_user a where a.status=1 ".$sqlCon)->queryScalar();
		$sql ="select @rownum := @rownum + 1 AS rank, ";
		$sql.="a.id, a.code, a.name, ";
		$sql.="case a.status when 1 then 'ใช้งานอยู่' else 'ยกเลิกใช้งาน' end as status, ";
		$sql.="b.name as lavel,c.name as createby ";
		$sql.="from mas_user a ";
		$sql.="left join mas_lavel b on a.lavel=b.id ";
		$sql.="left outer join mas_user c on c.id=a.create_by ";
		$sql.=",(SELECT @rownum := 0) r  ";
		$sql.="where a.status=1 ".$sqlCon ;
		
		//echo var_dump($sql);exit;
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'rank', 'id', 'code', 'name', 'facebook', 'line', 'status', 'lavel', 'pass','createby',
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
	
	public function getUser($id = null)
	{
		
	   	$sql ="select id,code, name, line, facebook, email,address,tel,pass, ";
		$sql.="lavel,status ";
	   	$sql.="from mas_user where id='".$id."' ";	   
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}

}	
