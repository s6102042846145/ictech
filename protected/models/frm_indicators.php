<?php

class frm_indicators extends CFormModel
{
	public $id;
	public $code;	
	public $name;	
	public $hai;	
	
	public function rules()
	{
		return array(
			array('hai', 'id','name', 'code', 'safe'),				
		);
	}

	public function attributeLabels()
	{
		return array(

		);
	}
	
	

	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
        
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."') ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
			
		
		$sql = "INSERT INTO ictech_mas_indicators (code,hai,name,create_date,create_by) VALUES(:code,:hai,:name,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":code", $this->code);	
		$command->bindValue(":hai", $this->hai);	
		$command->bindValue(":name", $this->name);		
		if($command->execute()) {
			$id = Yii::app()->db->getLastInsertID();
            $sql = 'call spSaveIndicatorsMonitor ('.$createby.','.$id.')' ;
            $command=yii::app()->db->createCommand($sql);		
            if($command->execute()) {              
                return true;
            } else { 
                Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
                return false;
            }		
			return true;
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_update()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		
		//save
		$sql ="select count(*) as aa from ictech_mas_indicators where status=1 and create_by=".$createby." and (name='".$this->name."' or code='".$this->code."') and id!='".$this->id."'";
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem)
        {
            if ($dataitem['aa']>0){
                Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
                return false;
            }
        }	
        $sql = "update ictech_mas_indicators set code=:code, hai=:hai,name=:name, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);
        //$command1->bindValue(":user_id", addslashes($this->user_id));
        $command->bindValue(":code", $this->code);
        $command->bindValue(":hai", $this->hai);
        $command->bindValue(":name", $this->name);				
        if($command->execute()) {
            return true;
        } else {
            Yii::app()->session['errmsg']='ไม่สามารถบันทึกข้อมูลได้';
            return false;
        }	
	}
	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;		

        $sql = "update ictech_mas_indicators set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
		
}
