<?php
class lkup_indicators extends CActiveRecord

{

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	public function tableName()

	{

		return 'mas_user';

	}



    public function attributeLabels() {

        return array(

        );

    }
    
    public static function getChkList()
	{
         $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
         $sqlCon=" and a.create_by=".$createby;	
         $sql=" select b.name dep_name , a.department_id,a.indicators_id,a.`status`,c.hai 
                from ictech_mas_department_monitor a
                left join ictech_mas_department b on a.department_id=b.id                
                left join ictech_mas_indicators c on a.indicators_id=c.id
                 where a.status!=0 ".$sqlCon." 
                ORDER BY b.id, c.id
                " ;
         $rows =Yii::app()->db->createCommand($sql)->queryAll();
	     return $rows;
     }
   public static function getIndicators2()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id, name, hai, aa from (
	select id, name, 0 hai, 1 aa from ictech_mas_department where status!=0 ".$sqlCon." 
	UNION all
	select id, name, hai, 2 aa from ictech_mas_indicators where status!=0 ".$sqlCon." 
) x ";	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
	
    public static function getIndicators()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select *,case when hai=0 then '-' else 'HAI/CAI' end haicai  from ictech_mas_indicators where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
        
    public static function getGetdata($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_indicators where status!=0 and id=".$id." and create_by=".$createby;	 
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	
}	

