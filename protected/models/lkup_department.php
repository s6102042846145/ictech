<?php





class lkup_department extends CActiveRecord

{

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	public function tableName()

	{

		return 'mas_user';

	}



    public function attributeLabels() {

        return array(

        );

    }
    /*
    public function getIndicators($id= null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $strSql = " from ictech_mas_department_monitor a ";
        $strSql.=" RIGHT  JOIN ictech_mas_indicators b on b.id=a.indicators_id and b.create_by=".$createby." and a.department_id='".$id."' " ;
        $strSql.=" where b.`status`!=0 ";
        
        $count=Yii::app()->db->createCommand('select count(*) '.$strSql)->queryScalar();
        $sql=" select b.code,b.name,b.id,case when IFNULL(a.indicators_id,0)=0 then '' else 'checked' end checked ".$strSql;	
        //echo var_dump($sql);exit();
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 'id', 'code', 'name'
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
    }	
    */
    public function getIndicators($id= null)
    {
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql=" select b.code,b.name,b.id,case when IFNULL(a.status,0)=0 then '' else 'checked' end checked ";
        $sql.=" from ictech_mas_department_monitor a ";
        $sql.=" RIGHT JOIN ictech_mas_indicators b on b.id=a.indicators_id ";
        $sql.=" left join ictech_mas_indicators c on a.indicators_id=c.id ";
        $sql.=" where c.`status`!=0  ";
        $sql.=" and b.create_by=".$createby ;
        $sql.=" and a.department_id=".$id ;
        
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
    }	
    
    
    public static function getSearch()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and create_by=".$createby;	
        $sql="select id,code,name from ictech_mas_department where status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    
    
    public static function getDepartment($id = null)
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select * from ictech_mas_department where id=".$id." and create_by=".$createby;	   
        $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
	}
	
}	

