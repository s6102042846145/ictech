<?php
class lkup_problemandsuggestion extends CActiveRecord

{

    public static function getSearch()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sqlCon=" and a.create_group=".$createby;	
        $sql="select a.id,a.name
                ,b.`name` department_name
                ,CONCAT(DATE_FORMAT( a.create_date , '%d' ),'/', DATE_FORMAT( a.create_date , '%m' ) ,'/',DATE_FORMAT( a.create_date , '%Y' ) +543) create_date 
                from ictech_tran_problemandsuggestion a
                left join ictech_mas_department b on a.department_id=b.id
                where a.status!=0 ".$sqlCon;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    
}	

