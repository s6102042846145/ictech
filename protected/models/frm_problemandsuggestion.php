<?php

class frm_problemandsuggestion extends CFormModel
{
	public $id;	
	public $name;	
	public $filename;	
	public $postdate;	
	public $filepath;	
    
	public function rules()
	{
		return array(
			array('id','name', '$filename','filepath', 'postdate'),				
		);
	}
	public function save_insert()
	{
		//check error
		//เช็คว่ามีข้อมูลหรือไม่
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
		$sql ="select count(*) as aa from ictech_tran_problemandsuggestion where status=1 and create_by=".$createby." and name='".$this->name."' ";
        
	   	$data =Yii::app()->db->createCommand($sql)->queryAll();
		foreach($data as $dataitem){
			if ($dataitem['aa']>0){
				Yii::app()->session['errmsg']='มีข้อมูลนี้ในระบบแล้ว';
				return false;
				}
			}		
		
		//save
		$sql = "INSERT INTO ictech_tran_problemandsuggestion (name,file_name,file_path,file_date,create_date,create_by) VALUES(:name,:file_name,:file_path,:file_date,now(),$createby)";
		$command=yii::app()->db->createCommand($sql);		
		$command->bindValue(":name", $this->name);	
		$command->bindValue(":file_name", $this->filename);	
		$command->bindValue(":file_path", $this->filepath);	
		$command->bindValue(":file_date", $this->postdate);		
		if($command->execute()) {
            $id = Yii::app()->db->getLastInsertID();           
			return true;            
		} else { 
			Yii::app()->session['errmsg']='เกิดข้อผิดพลาดบันทึกไม่สำเร็จ';
			return false;
		}			
	}	

	public function save_delete()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
        $sql = "update ictech_tran_problemandsuggestion set status=0, update_date=now(), update_by=$createby where id='".$this->id."'";
        $command=yii::app()->db->createCommand($sql);			
            if($command->execute()) {
                return true;
            } else {
                Yii::app()->session['errmsg']='ไม่สามารถลบข้อมูลได้';
                return false;
        }	
	}
		
}

