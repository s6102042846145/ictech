<?php


class lookupdata extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
	public static function getUserlogin($id = null)
	{
        $sql = 'call spGetUserlogin ('.$id.')' ;
        //echo var_dump("sql",$sql);exit;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
    
    
        
    public static function getDivision()
	{
        $createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;	
        $sql="select a.id,a.name,a.code,b.name department from ictech_mas_division a ";
        $sql.=" left join ictech_mas_department b on b.id=a.department_id";	
        $sql.=" where a.status!=0 and a.create_by=".$createby;	
	    $rows =Yii::app()->db->createCommand($sql)->queryAll();
	    return $rows;
		
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	public function getSample($id = null)
	{
	   $sql ="select code as code ,name as name
				from table where active=1
				order by code";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	
	
	
	
	
	
	//ab
	public function getUser($id = null)
	{
		
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
		$sqlCon = '';
		/*
		if(Yii::app()->user->getInfo('lavel')!=3){
		 	$sqlCon.= " and create_by=".$createby;
		}
		*/
		if($id!=''){
			$sqlCon.= " and id=".$id;
		}
	   //$sql ="select id ,name from mas_user where id='".$id."' and status!='0' order by id";
	   $sql =" select id ,name from mas_user where status='1' ".$sqlCon;
	   $sql.=" order by id";
	   //echo var_dump($sql);exit;
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getProduct()
	{
		$createby = !Yii::app()->user->isGuest?Yii::app()->user->id:0;
	   	$sql ="	select a.id ,a.name 
				from mas_product a
				where a.status!=0 and  a.id 
				in (select product_id from tran_rate WHERE status!=0 group by product_id) ";
		//$sql.=" and create_by=".$createby;
		$sql.=" order by a.id ";
			
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getRemark()
	{
	   	$sql ="	select id,name,totle,remark, ";
	   	$sql.=" case when detail=1 then 'VIP'  ";
		$sql.=" when detail=2 then 'Super vip'  ";
		$sql.=" when detail=3 then 'Dealer'  ";
		$sql.=" when detail=4 then 'Super Dealer'  ";
		$sql.=" else 'Platinum' end  detail ";
		$sql.=" from mas_bonus ";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getBonus($id=null)
	{
		/*
		$sql ="	SELECT * FROM mas_bonus ";
		$sql.=" where totle<=(select sum(total_amount) as total from tran_bill where user_id=".$id." )";
		//$sql.=" where totle<=10000 ";
		$sql.=" ORDER BY totle DESC LIMIT 1 ";
		//echo var_dump($sql);
		*/
		/*
		$sql ="	SELECT id as bonus_id FROM mas_bonus ";
 		$sql.="	where totle<=(select total from tran_bonus  where status=1 and user_id=".$id." ) ";
		*/
		
		$sql ="	SELECT a.id,a.bonus_id, b.name,a.`status` ";
		$sql.="	FROM tran_bonus a ";
		$sql.="	LEFT JOIN mas_bonus b on a.bonus_id=b.id ";
		$sql.="	WHERE a.status!=0 and a.user_id=".$id." ";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getBonusUser($id=null)
	{
		$sql ="	SELECT count(*) as aa FROM tran_bonus where status in (1,2) and user_id=".$id;
		//echo var_dump($sql);
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getGroup($id=null)
	{
		$userId="";
		if($id!=null){
			$userId.=" where id in (3,4) ";
		}
		$sql ="	SELECT id,name FROM mas_lavel ".$userId;
		//echo var_dump($sql);
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function  getCard()	
	{
		$sql ="	SELECT id,name FROM mas_card where status=1 ";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function showProduct()
	{
	/*
		$sql ="	SELECT a.name,a.img,b.sell_2  ";
		$sql.=" FROM mas_product a ";
		$sql.=" left join tran_product_price b on a.id=b.product_id ";
		$sql.=" where a.status!=0 and a.lavel=1 GROUP BY a.id";
		//echo var_dump($sql);
*/
		$sql ="	SELECT a.name,a.img,b.sell_2,b.create_by
		  FROM mas_product a  
		  left join tran_product_price b on a.id=b.product_id  
		left join mas_user c on c.id=a.create_by  
		  where a.status!=0 and c.lavel=1 GROUP BY a.id ";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function showProductHit()
	{
		/*
		$sql ="	SELECT b.`name`,b.img,c.sell_2  from(
				SELECT a.product_id,count(a.amountadd) as cnt
						 FROM tran_product_price a 
						 where a.status!=0 and a.create_by=2 and a.add=2
				GROUP BY a.product_id) a
				LEFT JOIN mas_product b on a.product_id=b.id
				left join tran_product_price c on c.product_id=a.product_id
				where cnt >1
				GROUP BY a.product_id";
				*/
		$sql ="	SELECT a.name,a.img,b.sell_2,b.create_by
		  FROM mas_product a  
		  left join tran_product_price b on a.id=b.product_id  
		left join mas_user c on c.id=a.create_by  
		  where a.status=2 and c.lavel=1 GROUP BY a.id ";		
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function showReview()
	{
		$sql ="	SELECT remark,img FROM tran_review 
						 where status=1 order by id desc limit 5";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function showReviewall()
	{
		$sql ="	SELECT remark,img FROM tran_review 
						 where status=1 order by id desc";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getAgent()
	{
		$count=Yii::app()->db->createCommand("select count(*) from mas_user a where a.status!=0 and a.lavel=2 ")->queryScalar();
		$sql ="select ";
		$sql.="a.id, a.code, a.name, a.facebook, a.line, a.tel, a.address ";
		$sql.="from mas_user a ";
		$sql.="where a.status!=0 and a.lavel=2 " ;
		
		//echo var_dump($sql);exit;
		return new CSqlDataProvider($sql, array(
			'totalItemCount'=>$count,
			'sort'=>array(
				'attributes'=>array(
					 
				),
			),
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['prg_ctrl']['pagination']['default']['pagesize'],
			),
		));	
	}
	
	public function showImg()
	{
	
		$sql ="	SELECT remark,img FROM tran_review where status=3";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function showSlide()
	{
	
		$sql ="	SELECT remark,img FROM tran_review where status=2";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function getYears()
	{
	   $sql =" SELECT YEAR(sell_date) id FROM `tran_bill` where status!=0 and ifnull(sell_date,'')!='' GROUP BY YEAR(sell_date) ";			
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*

	public function getDepartment($areaid=null)
	{
		
	   	$sql ="select id as id ,REPLACE(name,'\\\','') as name from mas_department where status=1 ";
		if($areaid!=null){
			$sql.="and area_id=".$areaid;	
		}
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();	  	
	   	return $rows;
	}
	public function getDepartmentgroup($id = null)
	{
		
	   $sql ="select a.id as id ,REPLACE(a.name,'\\\','') as name,b.area_id	from mas_departmentgroup a left join mas_department b on a.department_id=b.id where a.department_id='".$id."' and a.status = 1 ";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
		
	}
    */
	public function getBorrowdepartmentgroup()
	{
		
	   $sql ="select id as id ,REPLACE(name,'\\\','') as name	from mas_departmentgroup where status=1 ";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
		
	}

	public function getCompany()
	{
	   $sql ="select id as id ,REPLACE(name,'\\\','') as name	from mas_company where status=1 order by id";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	
	public function getType()
	{
	   $sql ="select id as id ,REPLACE(name,'\\\','') as name	from mas_assettype where status=1 order by id";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getBrand()
	{
	   $sql ="select id as id ,REPLACE(name,'\\\','') as name	from mas_assetbrand where status=1 order by id";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getModel()
	{
	   $sql ="select id as id ,REPLACE(name,'\\\','') as name	from mas_assetmodel where status=1 order by id";
	   $rows =Yii::app()->db->createCommand($sql)->queryAll();
	   return $rows;
	}
	public function getTemplatespec()
	{
		$sql ="select id as id ,REPLACE(name,'\\\','') as name from mas_templatespec where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	
	
	
	
	public function getContype($id = null)
	{	
	   	$sql ="select distinct a.id,REPLACE(a.name,'\\\','') as name from mas_assettype a ";
		$sql.="left join mas_assetspec b on b.assettype_id=a.id ";
		$sql.="left join mas_asset c on c.assetspec_id=b.id ";
		$sql.="where c.po_contract_id='".$id."' and b.status = 1";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
		
	}
	public function getStatus()
	{
		$sql ="select id,REPLACE(name,'\\\','') as name from mas_status order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getAcquiry()
	{
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_acquiry_method where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getPurchase()
	{
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_purchase_method where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getContractall()
	{
		$sql ="select id ,REPLACE(contract_no,'\\\','') as name from mas_contract where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getContract()
	{
		$sql ="select id ,REPLACE(contract_no,'\\\','') as name from mas_contract where contract_type='po' and status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getContractma()
	{
		$sql ="select id ,REPLACE(contract_no,'\\\','') as name from mas_contract where contract_type='ma' and status=1";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function getOs()
	{
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_repairos where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	
	public function getRepairproblem()
	{	
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_repairproblem where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getRepairreason()
	{	
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_repairreason where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getRepairsolution()
	{	
		$sql ="select id ,REPLACE(name,'\\\','') as name from mas_repairsolution where status=1 order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function getYear()
	{
		$sql ="select '0' as id, 'น้อยกว่า 1 ปี' as name union select '1' as id, '1 ปี' as name union select '2' as id, ";
		$sql.=" '2 ปี' as name union select '3' as id, '3 ปี' as name union select '4' as id, '4 ปี' as name union ";
		$sql.=" select '5' as id, '5 ปี' as name union select '6' as id, 'มากกว่า 5 ปี' as name ";
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getRepair()
	{
		//$sql = "select id ,REPLACE(document_no,'\\\','') as name, status, ";
		$sql = "select distinct status, ";
		$sql.= "case status when 'new' then 'แจ้งซ่อม' when 'receive' ";
		$sql.= "then 'รับเรื่อง' when 'repair' then 'ซ่อมแล้ว' when 'backup' then 'วางเครื่อง' when 'close' then 'ปิดงาน' end as statusname ";
		$sql.= "from trn_repair order by id";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}	

	public function getstatusBorrow($id = null)	
	{
		$sql = "select status ";
		$sql.= "from trn_borrow where id=".$id;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	
	public function getBorrowid($id = null)	
	{
		$sql = "select status ";
		$sql.= "from trn_borrow where id=(select borrow_id from trn_borrowitm where id =".$id.")";
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
	public function getStartdate($id = null)	
	{
		$sql = "select concat(date_format(plan_borrow_date_from,'%d/%m/'),date_format(plan_borrow_date_from,'%Y')+543)";
		$sql.= " as plan_borrow_date_from ";

		$sql.= "from trn_borrow where id=".$id;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
		public function getEnddate($id = null)	
	{
		$sql = "select concat(date_format(plan_borrow_date_to,'%d/%m/'),date_format(plan_borrow_date_to,'%Y')+543)";
		$sql.= " as plan_borrow_date_to ";
		$sql.= "from trn_borrow where id=".$id;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}
		public function getUsercreate($id = null)	
	{
		$sql = "select create_by ";
		$sql.= "from trn_borrowitm where borrow_id=".$id;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}

		public function getStatus_item($id = null)	
	{
		$sql = "select status ";
		$sql.= "from trn_borrowitm where borrow_id=".$id;
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}	
	
	
	public function getPmtype(){
		$sql ="select id,replace(name,'\\\','') as name from mas_pmtype where status=1 order by id ";	
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;		
	}
	
	public function getArea(){
		$sql ="select id,replace(name,'\\\','') as name from mas_area where status=1 order by id ";	
	   	$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;		
	}
	
	public function getOrder_return($id=null){
		$sql = "select return_order from trn_borrowitm where borrow_id=".$id." and return_order is not null ";
		$sql.= "group by return_order order by return_order ";	
		$rows =Yii::app()->db->createCommand($sql)->queryAll();
	   	return $rows;
	}	
	
	
		
}		