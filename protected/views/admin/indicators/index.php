<?php
	$this->pageTitle = 'ข้อมูลตัวชี้วัด' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
    <section class="content mt-3">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
        
            <div class="card">
                <div class="card-header thsarabunnew">
                <label class="card-title">ข้อมูลตัวชี้วัด</label>

                <div class="card-tools">
                  <ul class="pagination pagination-sm float-right">
                      <li class="page-item"><a class="page-link" id="btnAdd" href="javascript:void(0)">Add</a></li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped thsarabunnew">
                      <thead>
                      <tr>   
                        <th>รหัส</th>
                        <th>ชื่อตัวชี้วัด</th>
                        <th>HAI/CAI</th>
                        <th></th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                            <?php 
                                $data=lkup_indicators::getIndicators();
                              
                                foreach($data as $dataitem)
                                {
                                    echo '<tr>
                                            <td style="width: 100px;" >'.$dataitem['code'].'</td>
                                            <td class="w-100">'.$dataitem['name'].'</td>
                                            <td style="width: 100px;" >'.$dataitem['haicai'].'</td>
                                            <td class="text-center" style="width: 30px;"><span title="แก้ไข" onclick="setUpdate('.$dataitem['id'].')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span></td>
                                            <td class="text-center" style="width: 30px;"><span title="ลบ" class="badge bg-danger cursor-pointer" onclick="setDelete('.$dataitem['id'].')"><i class="fas fa-trash-alt"></i></span></td>
                                          </tr>';
                                } 
                              
                            ?>
                      </tbody>                  
                    </table>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 <div class="modal fade" id="modal-lg" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <label class="modal-title"></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-row">
              <div class="col-md-3"> 
                  <div class="position-relative form-group"> 
                      <span for="txtcode">รหัส</span>
                      <input id="txtcode" type="text" class="form-control">        
                  </div> 
              </div>
              <div class="col-md-7"> 
                  <div class="position-relative form-group"> 
                      <span for="txtname">ชื่อตัวชี้วัด</span>
                      <input id="txtname" type="text" class="form-control">        
                  </div> 
              </div>
              <div class="col-md-2">
                 <div class="position-relative form-group"> 
                    <input type="checkbox" id="checkboxHAI">
                    <label for="checkboxHAI">HAI/HCI</label>
                 </div> 
              </div>
           </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input id="hdfid" type="hidden" />
<script type="text/javascript">
  
    jQuery(document).ready(function ($) {        
        $('#btnAdd').click(function () {
            $(".modal-title").html("เพิ่มข้อมูล");  
            $("#modal-lg").modal('show');
         });
        $('#modaldetail').on('hidden.bs.modal', function (e) {
            $('#checkboxHAI').val('');
            $('#txtname').val('');
            $('#hdfid').val('');
        });

        
    });
   
    function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var hai= $('#checkboxHAI').is(":checked");// $('#checkboxHAI').val();
        var code=$('#txtcode').val();
        var name=$('#txtname').val();
	   //alert(hai);return;
        if(name=='')
        {
            alert('กรุณากรอกชื่อตัวชี้วัด');
            return;
        }       
        $("#modal-lg").modal('hide');
        $("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicators/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','code':code,'hai':hai==true ? 1: 0,'name':name,'id':id},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                   
                    window.location.reload();
                }
                else
                {
                    alert(data.msg);
                } 
            }
        });
    }	
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicators/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {                    
                    //$('#checkboxHAI').val(data.hai);
                    $( "#checkboxHAI" ).prop( "checked", data.hai==1 ? true:false );
                    $('#txtcode').val(data.code);	
                    $('#txtname').val(data.name);	
                    $('#hdfid').val(data.id);			
                   		
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/indicators/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {			
                        window.location.reload();                       
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
</script>
