<?php
	$this->pageTitle = 'ข้อมูลหน่วยงาน' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
    <section class="content mt-3">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
        
            <div class="card">
                <div class="card-header thsarabunnew">
                <label class="card-title">ข้อมูลหน่วยงาน</label>

                <div class="card-tools">
                  <ul class="pagination pagination-sm float-right">
                      <li class="page-item"><a class="page-link" id="btnAdd" href="javascript:void(0)">Add</a></li>
                  </ul>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped thsarabunnew">
                      <thead>
                      <tr>                    
                        <th>รหัส</th>
                        <th>ชื่อหน่วยงาน</th>
                        <th></th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                            <?php 
                                $data=lkup_department::getSearch();
                                foreach($data as $dataitem)
                                {
                                    echo '<tr>
                                            <td style="width: 100px;">'.$dataitem['code'].'</td>
                                            <td class="w-100">'.$dataitem['name'].'</td>
                                            <td class="text-center" style="width: 30px;"><span title="แก้ไข" onclick="setUpdate('.$dataitem['id'].')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span></td>
                                            <td class="text-center" style="width: 30px;"><span title="ลบ" class="badge bg-danger cursor-pointer" onclick="setDelete('.$dataitem['id'].')"><i class="fas fa-trash-alt"></i></span></td>
                                          </tr>';
                                } 
                            ?>
                      </tbody>                  
                    </table>
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 <div class="modal fade" id="modal-lg" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <label class="modal-title"></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-row">
              <div class="col-md-4"> 
                  <div class="position-relative form-group"> 
                      <span for="txtcode">รหัส</span>
                      <input id="txtcode" type="text" class="form-control">        
                  </div> 
              </div>
              <div class="col-md-8">
                  <div class="position-relative form-group"> 
                      <span for="txtname">ชื่อหน่วยงาน</span>
                      <input id="txtname" type="text" class="form-control">        
                  </div> 
              </div>
           </div>
           <div class="table-responsive">
              <table id="tbindicators" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ตัวชี้วัด</th>
                            <th>เลือก</th>
                            <th>เพิ่มเติม</th>
                        </tr>
                    </thead>

                </table>
            </div>
                    
            
            
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input id="hdfid" type="hidden" />

<script type="text/javascript">
    jQuery(document).ready(function ($) { 
        $('#btnAdd').click(function () {
            $(".modal-title").html("เพิ่มข้อมูล");  
            $("#modal-lg").modal('show');
            $("#tbindicators").addClass("d-none");
         });
        $("#modal-lg").on('hidden.bs.modal', function (e) {
            $("#exampleModalCenter").modal('show');
            window.location.reload();
        });
     
         
    });
  
    function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var code=$('#txtcode').val();
        var name=$('#txtname').val();
	   if(code=='')
       {
            alert('กรุณากรอกรหัสหน่วยงาน');
            return;
        }
        if(name=='')
        {
            alert('กรุณากรอกชื่อหน่วยงาน');
            return;
        }       
        $("#modal-lg").modal('hide');
        $("#exampleModalCenter").modal('show');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','code':code,'name':name,'id':id},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                   
                    window.location.reload();
                }
                else
                {
                    alert(data.msg);
                } 
            }
        });
    }	
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
        
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/departmentdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {                    
                    $('#txtcode').val(data.code);
                    $('#txtname').val(data.name);	
                    $('#hdfid').val(data.id);			
                    getIndicators(id);
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    function getIndicators(id) {   
       $("#tbindicators").removeClass("d-none");
		$('#tbindicators').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/indicators"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json"
            },
            "columns": [
                { "data": "code" },
                { "data": "name" },
                {
                    data:   "checked",
                    'render': function (data, type, full,type){
                         //return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                         return '<input type="checkbox" id="chk'+full.id+'" onclick="selectdata('+full.id+')" ' + data + ' />';
                     },
                    className: "dt-body-center"
                },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         //return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                         return '<a target="_bank" href="/admin/department/form?indicators='+full.id+'">รายละเอียด</a> ';
                     },
                    className: "dt-body-center"
                }
            ],
           pageLength: 10,
           "searching": false,
           "bLengthChange": false
        });
    }
    function selectdata(id){
        //$("#exampleModalCenter").modal('show');
        var status = $('#chk'+ id).is(":checked");
        //var chk = $("#chk".id).is(':checked');
        var department_id = $('#hdfid').val();
       //alert(status);return;
         $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/setdata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','indicators_id':id,'department_id':department_id,'status':status==true ? 1: 0},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {			
                        $("#exampleModalCenter").modal('hide');                          
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/department/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {			
                        window.location.reload();                       
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
</script>
