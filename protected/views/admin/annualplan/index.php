<?php
	$this->pageTitle = 'แผนประจำปี' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
<section class="row content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">ข้อมูลแผนประจำปี</label>

            <div class="card-tools">
              <ul class="pagination pagination-sm float-right">
                  <li class="page-item"><a class="page-link" id="btnAdd" href="javascript:void(0)">Add</a></li>
              </ul>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ไฟล์</th>
                            <th style="width:150px;">วันที่ประกาศ</th>
                            <th style="width:50px;" class="text-center">แก้ไข</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    

 <div class="modal fade" id="modal-lg" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header thsarabunnew">
          <label class="modal-title"></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body thsarabunnew">
          <div class="form-row">
              <div class="col-md-8"> 
                  <div class="position-relative form-group"> 
                      <span for="txtname">ชื่อ</span>
                      <input id="txtname" type="text" class="form-control">        
                  </div> 
              </div>
              <div class="col-md-4">
                  <div class="position-relative form-group"> 
                      <span for="txtpostdate">วันที่ประกาศ</span>
                      <div class="input-group">   
                        <input type="text" id="txtpostdate" class="form-control" autocomplete="off" >
                        <div class="input-group-append cursor-pointer">
                            <div class="input-group-text" onClick="$('#txtpostdate').datepicker('show');"><i class="fa fa-calendar"></i></div>
                        </div>
                      </div>       
                  </div> 
              </div>
           </div>
           <div class="form-group"> 
                <span for="paperclip">ไฟล์</span>
               
                <div class="input-group">       
                    
                    <input id="paperclip" type="file"  name="paperclip"accept=".pdf" class="d-none" />
                    <input type="text" id="txtfilename" class="form-control upload" disabled>
                    <div class="input-group-append">
                        <div class="input-group-text" onclick="btclick()"><i class="fas fa-file-upload"></i></div>
                    </div>
                </div>   
           </div>
           
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onClick="ajax_savedata();">Save</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>                     
<input id="hdfid" type="hidden" />                           
<input id="hdffilepath" type="hidden" />  


<script type="text/javascript">
    jQuery(document).ready(function ($) { 
        document.getElementById('paperclip').onchange = function () {
            $("#txtfilename").val(this.files.item(0).name);
        };
        $("#txtpostdate").datepicker({
            language:'th-th',
            format:'dd/mm/yyyy',
            autoclose: true
        });
        
        $("#modal-lg").on('hidden.bs.modal', function (e) {
            $('#hdfid').val("");
            $('#txtname').val("");
            $('#hdffilepath').val("");
            $('#txtfilename').val("");
            $('#txtpostdate').val("");
        });
     
       
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/search"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':1},
                dataType: "json"
            },
            "columns": [
                { "data": "name" },
                { "data": "file_name" },
                { "data": "file_date" },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setUpdate('+full.id+')" class="badge bg-primary cursor-pointer"><i class="fas fa-edit"></i></span>';
                     },
                    className: "dt-body-center"
                },
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="แก้ไข" onclick="setDelete('+full.id+')" class="badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
            ],
           //pageLength: 10,
           //"bLengthChange": false
        });
        $('#btnAdd').click(function () {
            $(".modal-title").html("เพิ่มข้อมูล");  
            $("#modal-lg").modal('show');
        });
        
       
    });
    function setUpdate(id) {     
        $(".modal-title").html("แก้ไขข้อมูล");  
        $("#modal-lg").modal('show');
        $("#exampleModalCenter").modal('show');
         $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/getdata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
            dataType: "json",				
            success: function (data) {
                if (data.status=='success') {   
                    $('#hdfid').val(data.id);	                 
                    $('#txtname').val(data.name);
                    $('#hdffilepath').val(data.filepath);	
                    $('#txtfilename').val(data.filename);
                    //$('#txtpostdate').val(data.postdate);	
                    var from = data.postdate.split("/")
                    var f = new Date(from[2]-543, from[1] - 1, from[0])
                    $('#txtpostdate').datepicker('setDate', f);
                    $("#exampleModalCenter").modal('hide');
                    
                }else{
                    alert(data.msg);
                } 
            }
        });	
    }
    async function ajax_savedata() 
    {
        var id=$('#hdfid').val();
        var name=$('#txtname').val();
        var filename=$('#txtfilename').val();
        var postdate=$('#txtpostdate').val();
        var filepath ="";
        
        if(name=='')
        {
            alert('กรุณากรอกชื่อ');
            return;
        }    
        if(filename=='')
        {
            alert('กรุณาเลือกไฟล์');
            return;
        }
         if(postdate=='')
        {
            alert('กรุณาเลือกวันที่');
            return;
        }
        const myArray = postdate.split("/");
        let d = myArray[0];
        let m = myArray[1];
        let y = myArray[2]-543;
        
        postdate = y+"-"+m+"-"+d;
        
        
            
        $("#modal-lg").modal('hide');
        $("#exampleModalCenter").modal('show');
         let formData = new FormData(); 
        formData.append("file", paperclip.files[0]);
        formData.append("path", "<?php echo Yii::app()->user->id; ?>");
        await fetch('/upload.php', {
            method: "POST", 
            body: formData
        }).then(response => response.json()).then(data => {
           filepath = data;
        }); 
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/savedata"); ?>",
            data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','postdate':postdate,'filename':filename,'filepath':filepath,'name':name,'id':id},
            dataType: "json",				
            success: function (data) 
            {
                if (data.status=='success') {                     
                    $('#tbdata').DataTable().ajax.reload();
                    $("#exampleModalCenter").modal('hide');
                }
                else
                {
                    alert(data.msg);
                    $("#exampleModalCenter").modal('hide');
                } 
            }
        });
    }	
    
    
    
    
    function btclick(){
	   $("#paperclip").click();
    }
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/annualplan/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        $("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    
</script>