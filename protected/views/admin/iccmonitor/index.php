<?php
	$this->pageTitle = 'ICC Monitor' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<style>
th 
{
  vertical-align: bottom !important;
  text-align: center;
}

th span 
{
  -ms-writing-mode: tb-rl;
  -webkit-writing-mode: vertical-rl;
  writing-mode: vertical-rl;
  transform: rotate(180deg);
  white-space: nowrap;
}
.div-center {
  margin: auto;
  width: 50%;
  padding: 10px;
}
    
</style>

 <?php 
    $data=lkup_indicators::getIndicators2();
    $data2=lkup_indicators::getChkList();
    if(count($data)!=0 && count($data2)!=0){
    

 ?>
<section class="content">
  <div class="container-fluid">
    <div class="row mt-3">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">ICCMonitor</h3>

            <div class="card-tools">
              <div class="input-group date" id="reservationdate" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
              <table class="table-bordered table table-hover text-nowrap thsarabunnew">
                    <?php 
                       

                        $rowstr = "<tr>";
                        $rowsth = "";
                        $rows2th = "";
                        $rowsendtr = "</tr>";
                        $rowBody="";
                        $rowBody2="";

                        $i=0;

                        foreach($data as $dataitem)
                        {
                            if($dataitem['aa']!=1){
                                 if($dataitem['hai']==0){ 
                                     $i++;
                                    $rowsth.=' <th rowspan="2" ><span>'.$dataitem['name'].'</span></th>';
                                }else{      
                                     $i=$i+2;
                                    $rowsth.=' <th colspan="2"><span>'.$dataitem['name'].'</span></th>';
                                    $rows2th.=' <th>HAI</th><th>CAI</th>';
                                }                     
                            }            
                        } 

                        $id=0;
                        for ($xx = 0; $xx < count($data2); $xx++) 
                        { 
                            $param = $data2[$xx]['status']=="0" ? '<i class="fas fa-question-circle text-secondary"></i>':'<i class="fas fa-times-circle text-danger"></i>';
                            if($id!=$data2[$xx]["department_id"]){
                                $id=$data2[$xx]["department_id"];
                                 $rowBody.='<tr class="text-center"><td class="text-left">'.$data2[$xx]['dep_name'].'</td><td>'.$param.'</td>';
                                if($data2[$xx]['hai']==1){   
                                   
                                    $rowBody.='<td>'.$param.'</td>';
                                }
                            }else{
                                if($data2[$xx]['hai']==1){   
                                    
                                    $rowBody.='<td>'.$param.'</td>';
                                }                    
                                $rowBody.='<td>'.$param.'</td>';
                                //$rowBody.='</tr>';
                            }     
                        }

                        echo "<thead>".$rowstr."<th rowspan='2'><div class='float-left mb-5'>หน่วยงาน </div><div class='float-right mt-5'>ตัวชี้วัด</div></th>".$rowsth.$rowsendtr.$rowstr.$rows2th.$rowsendtr."</thead>";
                        echo $rowBody;
                    ?>
                </table>   
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
    <!-- /.row -->

  </div><!-- /.container-fluid -->
</section>
<div class="col-md-4 thsarabunnew">
    <!-- Bootstrap Switch -->
    <div class="card card-secondary">
      <div class="card-header">
        <h3 class="card-title">หมายเหตุ</h3>
      </div>
      <div class="card-body">
          <label class="col-form-label"><i class="fa-check-circle fas text-success"></i> ลงข้อมูลแล้ว</label><br/>
          <label class="col-form-label"><i class="fas fa-times-circle text-danger"></i> ยังไม่ลงข้อมูลรายวัน</label><br/>
          <label class="col-form-label"><i class="fas fa-question-circle text-secondary"></i> ไม่มีตัวชี้วัดที่ต้องลงข้อมูล</label><br/>
          <label class="col-form-label"><i class="fas fa-radiation-alt text-yellow"></i> พบการติดเชื้อ/การปฏิบัติที่ไม่ถูกต้อง</label>
          

      </div>
    </div>
    <!-- /.card -->
</div>
<?php
    }else{
        echo "<div class='text-center div-center'>No data !</div>";
    }
?>
<script>
     jQuery(document).ready(function ($) {    
         
         $("div.container").eq(1).removeClass("container");
     });
</script>
