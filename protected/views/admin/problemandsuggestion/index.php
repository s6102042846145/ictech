<?php
	$this->pageTitle = 'ปัญหา/ข้อเสนอแนะ' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>


 <!-- Main content -->
<section class="row content mt-3">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
            <div class="card-header thsarabunnew">
            <label class="card-title">ปัญหา/ข้อเสนอแนะ</label>

          </div>
          <!-- /.card-header -->
          <div class="card-body">
              <div class="table-responsive">
                <table id="tbdata" class="w-100 table table-bordered table-striped thsarabunnew">
                    <thead>
                        <tr>
                            <th>วันที่</th>
                            <th style="width:150px;">หน่วยงาน</th>
                            <th>ประเด็นปัญหา/ต้องการให้ทีม ICC ช่วยเหลือ</th>
                            <th style="width:50px;" class="text-center">ลบ</th>
                        </tr>
                    </thead>

                </table>
              </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>    
               
<input id="hdfid" type="hidden" />        


<script type="text/javascript">
    jQuery(document).ready(function ($) {         
       
        $('#tbdata').DataTable( {
            "ajax": {
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/problemandsuggestion/search"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':1},
                dataType: "json"
            },
            "columns": [
                { "data": "create_date" },
                { "data": "department_name" },
                { "data": "name" },               
                {
                    data:   "id",
                    'render': function (data, type, full,type){
                         return '<span title="ลบ" onclick="setDelete('+full.id+')" class="badge bg-danger cursor-pointer"><i class="fas fa-trash-alt"></i></span>';
                     },
                    className: "dt-body-center"
                }
            ],
           //pageLength: 10,
           //"bLengthChange": false
        });
       
    });
    
    
    function setDelete(id) {   
        var r = confirm("คุณต้องการลบข้อมูลนี้ใช่หรือไม่ !");

        if (r == true) {
            $("#exampleModalCenter").modal('show');
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createAbsoluteUrl("/admin/problemandsuggestion/deletedata"); ?>",
                data: {'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>','id':id},
                dataType: "json",				
                success: function (data) {
                    if (data.status=='success') {	
                        $('#tbdata').DataTable().ajax.reload();
                        $("#exampleModalCenter").modal('hide');
                    }
                    else{
                        alert(data.msg);
                    } 
                }
            });
        }
    }	
    
</script>