<?php
	$this->pageTitle = 'โครงสร้างทีม ICC' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<div class="offset-sm-top-60 text-left mb-5">
                <div class="tabs-custom tabs-vertical tabs-corporate" id="tabs-2">
                  <!--Nav tabs-->
                  <ul class="nav nav-tabs">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-2-1" data-toggle="tab">ปี 2565</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-2-2" data-toggle="tab">ปี 2564</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-2-3" data-toggle="tab">ปี 2563</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-2-4" data-toggle="tab">ปี 2562</a></li>
                  </ul>
                  <!--Tab panes-->
                  <div class="tab-content">
                    <div class="tab-pane fade active show" id="tabs-2-1">
                     
                        <img src="<?php echo Yii::app()->params['prg_ctrl']['chart'] ?>2565.png" style="width:100%" />
                    </div>
                    <div class="tab-pane fade" id="tabs-2-2">
                      <img src="<?php echo Yii::app()->params['prg_ctrl']['chart'] ?>2564.png" style="width:100%" />
                    </div>
                    <div class="tab-pane fade" id="tabs-2-3">
                     <img src="<?php echo Yii::app()->params['prg_ctrl']['chart'] ?>2565.png" style="width:100%" />
                    </div>
                    <div class="tab-pane fade" id="tabs-2-4">
                     <img src="<?php echo Yii::app()->params['prg_ctrl']['chart'] ?>2564.png" style="width:100%" />
                    </div>
                  </div>
                </div>
              </div>