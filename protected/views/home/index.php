<?php
	$this->pageTitle = 'หน้าหลัก' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>

      <section class="section swiper-container swiper-slider bg-default swiper-container-fade swiper-container-initialized swiper-container-horizontal" data-swiper="{&quot;autoplay&quot;:{&quot;delay&quot;:5000},&quot;effect&quot;:&quot;fade&quot;}">
        <div class="swiper-wrapper text-center" style="transition-duration: 0ms;"><div class="swiper-slide swiper-slide-duplicate swiper-slide-next swiper-slide-duplicate-prev" data-slide-bg="/images/slide-03.jpg" data-swiper-slide-index="2" style="width: 1519px; transition-duration: 0ms; opacity: 1; transform: translate3d(0px, 0px, 0px); background-image: url(&quot;/images/slide-03.jpg&quot;); background-size: cover;">
            <div class="swiper-caption">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-7 section-slider-custom">
                      <div class="text-lg-left">
                        <h2>Qualified Team<br class="d-block">of Experts</h2>
                        <h5 class="d-none d-lg-block mw-400">Our team of diagnosticians is always ready to help you be healthier.</h5><a class="btn btn-ellipse btn-white offset-top-24" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide swiper-slide-duplicate-active" id="page-loader" data-slide-bg="/images/slide-01.jpg" data-swiper-slide-index="0" style="width: 1519px; transition-duration: 0ms; opacity: 1; transform: translate3d(-1519px, 0px, 0px); background-image: url(&quot;/images/slide-01.jpg&quot;); background-size: cover;">
            <div class="swiper-caption">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-7 section-slider-custom">
                      <div class="inset-xl-right-80 text-lg-left">
                        <h2>Take Care of<br class="d-block"> Your Health
                        </h2>
                        <h5 class="d-none d-lg-block mw-400">At Medina, we are dedicated to diagnosing all kinds of diseases.</h5><a class="btn btn-ellipse btn-white offset-top-24" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-slide-bg="/images/slide-02.jpg" data-swiper-slide-index="1" style="width: 1519px; transition-duration: 0ms; opacity: 1; transform: translate3d(-3038px, 0px, 0px); background-image: url(&quot;/images/slide-02.jpg&quot;); background-size: cover;">
            <div class="swiper-caption">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-6 section-slider-custom to-front">
                      <div class="text-lg-left">
                        <h2>Years of<br class="d-block"> Experience
                        </h2>
                        <h5 class="d-none d-lg-block mw-400">Since our foundation, we've been delivering diagnostic solutions.</h5><a class="btn btn-ellipse btn-white offset-top-24" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide swiper-slide-prev swiper-slide-duplicate-next" data-slide-bg="/images/slide-03.jpg" data-swiper-slide-index="2" style="width: 1519px; transition-duration: 0ms; opacity: 1; transform: translate3d(-4557px, 0px, 0px); background-image: url(&quot;/images/slide-03.jpg&quot;); background-size: cover;">
            <div class="swiper-caption">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-7 section-slider-custom">
                      <div class="text-lg-left">
                        <h2>Qualified Team<br class="d-block">of Experts</h2>
                        <h5 class="d-none d-lg-block mw-400">Our team of diagnosticians is always ready to help you be healthier.</h5><a class="btn btn-ellipse btn-white offset-top-24" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="swiper-slide swiper-slide-duplicate swiper-slide-active" id="page-loader" data-slide-bg="/images/slide-01.jpg" data-swiper-slide-index="0" style="width: 1519px; transition-duration: 0ms; opacity: 1; transform: translate3d(-6076px, 0px, 0px); background-image: url(&quot;/images/slide-01.jpg&quot;); background-size: cover;">
            <div class="swiper-caption">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-7 section-slider-custom">
                      <div class="inset-xl-right-80 text-lg-left">
                        <h2>Take Care of<br class="d-block"> Your Health
                        </h2>
                        <h5 class="d-none d-lg-block mw-400">At Medina, we are dedicated to diagnosing all kinds of diseases.</h5><a class="btn btn-ellipse btn-white offset-top-24" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">make an appointment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div></div>
        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
      <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></section>
      <!--Sheldue-->
      <section class="bg-default-liac bg-white-liac section section-md pt-xl-0">
        <div class="container section-top-34 section-lg-top-0">
          <div class="row no-gutters justify-content-sm-center justify-content-xl-start offset-lg-top-34-negative sheldue text-sm-left to-front row-30">
            <div class="col-md-8 col-lg-5 col-xl-3">
              <div class="sheldue-item first">
                <div class="sheldue-item-body">
                  <div class="icon icon-xs icon-emergency-01 text-white-50"></div>
                  <h6 class="d-inline-block inset-left-10 text-white">PLAN S</h6>
                  <hr>
                  <div class="offset-top-24 text-gray-light thsarabunnew">
                    <p>สำหรับโรงพยาบาลขนาดเล็ก</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
                  </div>
                </div>
                <div class="sheldue-more"><a class="btn btn-white btn-block btn-rect thsarabunnew" href="#">รายละเอียด</a></div>
              </div>
            </div>
            <div class="col-md-8 col-lg-5 col-xl-3">
              <div class="sheldue-item light">
                <div class="sheldue-item-body">
                  <div class="icon icon-xs icon-emergency-01 text-white-50"></div>
                  <h6 class="d-inline-block inset-left-10 text-white">PLAN M</h6>
                  <hr>
                  <div class="offset-top-24 text-gray-light thsarabunnew">
                    <p>สำหรับโรงพยาบาลขนาดกลาง</p>
					<p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
                  </div>
                </div>
                <div class="sheldue-more "><a class="btn btn-white btn-block btn-rect thsarabunnew" href="#">รายละเอียด</a></div>
              </div>
            </div>
            <div class="col-md-8 col-lg-5 col-xl-3">
              <div class="sheldue-item">
                <div class="sheldue-item-body">
                  <div class="icon icon-xs icon-emergency-01 text-white-50"></div>
                  <h6 class="d-inline-block inset-left-10 text-white">PLAN L</h6>
                  <hr>
                  <div class="offset-top-24 text-gray-light thsarabunnew">
                    <p>สำหรับโรงพยาบาลขนาดใหญ่</p>
					<p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
                  </div>
                </div>
                <div class="sheldue-more "><a class="btn btn-white btn-block btn-rect thsarabunnew" href="#">รายละเอียด</a></div>
              </div>
            </div>
            <div class="col-md-8 col-lg-5 col-xl-3">
              <div class="sheldue-item light last">
                <div class="sheldue-item-body">
                  <div class="icon icon-xs icon-emergency-01 text-white-50"></div>
                  <h6 class="d-inline-block inset-left-10 text-white">PLAN XL</h6>
                  <hr>
                  <div class="offset-top-24 text-gray-light thsarabunnew">
                    <p>สำหรับโรงพยาบาลขนาดใหญ่พิเศษ</p>
					<p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
					  <p>- ทดสอบ 1</p>
                  </div>
                </div>
                <div class="sheldue-more "><a class="btn btn-white btn-block btn-rect thsarabunnew" href="#">รายละเอียด</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--What makes us different-->
      <section class="section-lg bg-default-liac">
        <div class="container text-center text-lg-left">
          <h3>What Makes Us Different</h3>
          <div class="row offset-top-60 row-50">
            <div class="col-md-6 col-lg-4">
              <div class="icon icon-xlg icon-circle icon-default icon-pills-xl"></div>
              <h5 class="font-weight-bold text-gray-darkest">Qualified Specialists</h5>
              <p>We hire the best specialists to deliver top-notch diagnostic services for you.</p>
            </div>
            <div class="col-md-6 col-lg-4 offset-md-top-0">
              <div class="icon icon-xlg icon-circle icon-default icon-doctor-xl"></div>
              <h5 class="font-weight-bold text-gray-darkest">Modern Equipment</h5>
              <p>We use the first-class medical equipment for timely diagnostics of various diseases.</p>
            </div>
            <div class="col-md-6 col-lg-4 offset-md-top-0">
              <div class="icon icon-xlg icon-circle icon-default icon-medical-car-xl"></div>
              <h5 class="font-weight-bold text-gray-darkest">Emergency Diagnostics</h5>
              <p>Our emergency diagnostics services help you get the most accurate diagnosis in a minimal time.</p>
            </div>
          </div>
        </div>
      </section>
      <!-- Material Parallax-->
      <section class="parallax-container" data-parallax-img="images/background-03-1920x939.jpg"><div class="material-parallax parallax"><img src="images/background-03-1920x939.jpg" alt="" style="display: block;"></div>
        <div class="parallax-content section-98 section-sm-110 context-dark">
          <div class="container text-left">
            <div class="row justify-content-sm-center justify-content-lg-start">
              <div class="col-md-10 col-lg-8 col-xl-5">
                <h2>All Kinds<br class="d-none d-xl-inline-block">of Diagnostics</h2>
                <p class="offset-top-30 text-white">Medina offers the region’s most comprehensive range of diagnostic services, from MRI to X-ray.</p>
                <div class="offset-top-30"><a class="btn btn-ellipse btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">free consultation</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- main services-->
      <section class="section-lg bg-default section">
        <div class="container">
          <h3 class="text-center">Our Services</h3>
          <div class="offset-top-41">
            <p class="custom-paragraph">At our clinic, you can experience the best and the most extensive range of diagnostic services in the state. Feel free to browse our website for more information.</p>
          </div>
          <div class="row offset-top-60 text-lg-left row-30">
            <div class="col-md-6 col-lg-4">
              <div class="service"><img class="img-responsive" src="images/home-01-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">Pediatrics</a>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 offset-sm-top-0">
              <div class="service"><img class="img-responsive" src="images/home-02-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">MRI of the brain</a>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 offset-md-top-0">
              <div class="service"><img class="img-responsive" src="images/home-03-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">x-ray diagnostic</a>
              </div>
            </div>
            <div class="col-md-6 col-lg-4">
              <div class="service"><img class="img-responsive" src="images/home-04-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">laboratory services</a>
              </div>
            </div>
            <div class="col-md-6 col-lg-4">
              <div class="service"><img class="img-responsive" src="images/home-05-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">pregnancy</a>
              </div>
            </div>
            <div class="col-md-6 col-lg-4">
              <div class="service"><img class="img-responsive" src="images/home-06-320x320.jpg" width="320" height="320" alt=""><a class="service-desc h6" href="https://ld-wt73.template-help.com/wt_prod-20176/services.html">Best Equipment</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Our Team-->
      <section class="section section-md p-xl-0">
        <div class="container-fluid">
          <div class="justify-content-sm-center row-30 row">
            <!-- Thumbnail Josip-->
            <figure class="thumbnail-josip thumbnail-big"><a href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html"><img width="384" height="410" src="images/our-team-05-384x410.jpg" alt=""></a>
              <div class="thumbnail-desc">
                <h5 class="thumbnail-josip-title text-medium text-white">Dr. Scott Riley</h5>
                <p class="d-none d-lg-block font-italic text-white offset-top-0">Chief Medical Officer, Pathologist</p>
              </div>
              <figcaption><a class="btn btn-block btn-rect text-center text-lg-left btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html">view full profile</a></figcaption>
            </figure>
            <!-- Thumbnail Josip-->
            <figure class="thumbnail-josip thumbnail-big"><a href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html"><img width="384" height="410" src="images/our-team-06-384x410.jpg" alt=""></a>
              <div class="thumbnail-desc">
                <h5 class="thumbnail-josip-title text-medium text-white">Dr. Jane Fowler</h5>
                <p class="d-none d-lg-block font-italic text-white offset-top-0">Clinical Laboratory Technologist</p>
              </div>
              <figcaption><a class="btn btn-block btn-rect text-center text-lg-left btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html">view full profile</a></figcaption>
            </figure>
            <!-- Thumbnail Josip-->
            <figure class="thumbnail-josip thumbnail-big offset-md-top-0 odd"><a href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html"><img width="384" height="410" src="images/our-team-07-384x410.jpg" alt=""></a>
              <div class="thumbnail-desc">
                <h5 class="thumbnail-josip-title text-medium text-white">Dr. Eric Snyder</h5>
                <p class="d-none d-lg-block font-italic text-white offset-top-0">MRI Technologist</p>
              </div>
              <figcaption><a class="btn btn-block btn-rect text-center text-lg-left btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html">view full profile</a></figcaption>
            </figure>
            <!-- Thumbnail Josip-->
            <figure class="thumbnail-josip thumbnail-big offset-md-top-0"><a href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html"><img width="384" height="410" src="images/our-team-08-384x410.jpg" alt=""></a>
              <div class="thumbnail-desc">
                <h5 class="thumbnail-josip-title text-medium text-white">Dr. Martha Schmidt</h5>
                <p class="d-none d-lg-block font-italic text-white offset-top-0">EKG Technician</p>
              </div>
              <figcaption><a class="btn btn-block btn-rect text-center text-lg-left btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html">view full profile</a></figcaption>
            </figure>
            <!-- Thumbnail Josip-->
            <figure class="thumbnail-josip thumbnail-big "><a href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html"><img width="384" height="410" src="images/our-team-09-384x410.jpg" alt=""></a>
              <div class="thumbnail-desc">
                <h5 class="thumbnail-josip-title text-medium text-white">Dr. James Wilson</h5>
                <p class="d-none d-lg-block font-italic text-white offset-top-0">Radiology Technician</p>
              </div>
              <figcaption><a class="btn btn-block btn-rect text-center text-lg-left btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/team-member.html">view full profile</a></figcaption>
            </figure>
          </div>
        </div>
      </section>
      <!-- testimonials-->
      <section class="section-lg bg-default-liac">
        <div class="container">
          <h3 class="text-center">Testimonials</h3>
          <div class="offset-top-66">
            <div class="owl-carousel owl-carousel-default owl-carousel-class-light owl-loaded" data-loop="false" data-items="1" data-dots="true" data-mouse-drag="false" data-lg-items="2" data-nav="false" data-owl="{&quot;dots&quot;:true,&quot;nav&quot;:false,&quot;autoplayTimeout&quot;:3500}" style="">
              
              
              
              
            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1200px, 0px, 0px); transition: all 0.25s ease 0s; width: 2400px;"><div class="owl-item" style="width: 570px; margin-right: 30px;"><div class="item">
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I had a colonoscopy at Medina Diagnostic Center. From the moment I entered the center, I was greeted and treated warmly and respectfully and it was the best experience.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="/images/user-betty-wade-60x60.jpg" alt=""></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize font-weight-bold text-primary">
                          <cite class="text-normal">Betty Wade</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div></div><div class="owl-item" style="width: 570px; margin-right: 30px;"><div class="item">
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I would like to thank and compliment the staff at Medina, including the outpatient, laboratory staff and cardiac division during my stay at your facility on July 9th and 10th.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="/images/user-bryan-green-60x60.jpg" alt=""></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize font-weight-bold text-primary">
                          <cite class="text-normal">Bryan Green</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div></div><div class="owl-item active" style="width: 570px; margin-right: 30px;"><div class="item">
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I had a colonoscopy at Medina Diagnostic Center. From the moment I entered the center, I was greeted and treated warmly and respectfully and it was the best experience.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="/images/user-bryan-green-60x60.jpg" alt=""></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize font-weight-bold text-primary">
                          <cite class="text-normal">Bryan Green</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div></div><div class="owl-item active" style="width: 570px; margin-right: 30px;"><div class="item">
                <blockquote class="quote quote-classic">
                  <div class="quote-body">
                    <p class="font-italic text-gray-dark">
                      <q>I would like to thank and compliment the staff at Medina, including the outpatient, laboratory staff and cardiac division during my stay at your facility on July 9th and 10th.</q>
                    </p>
                    <div class="quote-meta unit flex-row unit-spacing-sm align-items-center">
                      <div class="unit-left unit-item-narrow"><img class="rounded-circle quote-img" width="60" height="60" src="/images/user-betty-wade-60x60.jpg" alt=""></div>
                      <div class="unit-body unit-item-wide">
                        <h5 class="quote-author text-capitalize font-weight-bold text-primary">
                          <cite class="text-normal">Betty Wade</cite>
                        </h5>
                        <p class="quote-desc text-capitalize text-gray font-italic">Patient</p>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div></div>
          </div>
        </div>
        <!-- RD Parallax-->
      </section>
      <section class="parallax-container" data-parallax-img="/images/background-04-1920x980.jpg"><div class="material-parallax parallax"><img src="/images/background-04-1920x980.jpg" alt="" style="display: block;"></div>
        <div class="parallax-content section-98 section-sm-110 context-dark">
          <div class="container text-left">
            <div class="row justify-content-sm-center justify-content-lg-start">
              <div class="col-md-10 col-lg-8 col-xl-5">
                <h2>Pediatric<br class="d-none d-xl-inline-block">Diagnostics</h2>
                <p class="offset-top-30 text-white">Our center provides a vast range of pediatric diagnostic services for children all over our state.</p>
                <div class="offset-top-30"><a class="btn btn-ellipse btn-white" href="https://ld-wt73.template-help.com/wt_prod-20176/make-an-appointment.html">free consultation</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- latest blog posts-->
      <section class="section-98 section-sm-110">
        <div class="container">
          <h3 class="text-center text-lg-left">Latest Blog Posts</h3>
          <div class="row justify-content-sm-center offset-top-60">
            <div class="col-md-8 col-lg-4">
              <!-- Post Modern-->
              <article class="post post-modern post-modern-classic">
                <!-- Post media-->
                <div class="post-media"><a class="link-image" href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html"><img class="img-responsive img-cover" width="370" height="250" src="/images/post-13-370x250.jpg" alt=""></a>
                </div>
                <!-- Post content-->
                <div class="post-content text-left">
                  <!-- Post Title-->
                  <div class="post-title offset-top-8">
                    <h5 class="font-weight-bold"><a href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html">Reasons to Visit a Breast Specialist</a></h5>
                  </div>
                  <ul class="list-inline list-inline-dashed">
                    <li>June 21, 2021 at 8:12pm</li>
                    <li><a class="text-primary" href="https://ld-wt73.template-help.com/wt_prod-20176/blog-masonry.html">News</a></li>
                  </ul>
                  <!-- Post Body-->
                  <div class="post-body">
                    <p>There are a lot of women that are unaware of the numerous risks associated with their health and eventually ignore the importance of visiting...</p>
                  </div>
                  <div class="tags group group-sm">
                  </div>
                </div>
              </article>
            </div>
            <div class="col-md-8 col-lg-4 offset-top-50 offset-md-top-0">
              <!-- Post Modern-->
              <article class="post post-modern post-modern-classic">
                <!-- Post media-->
                <div class="post-media"><a class="link-image" href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html"><img class="img-responsive img-cover" width="370" height="250" src="/images/post-14-370x250.jpg" alt=""></a>
                </div>
                <!-- Post content-->
                <div class="post-content text-left">
                  <!-- Post Title-->
                  <div class="post-title offset-top-8">
                    <h5 class="font-weight-bold"><a href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html">Picking the Right Diagnostic Services for Efficient Results</a></h5>
                  </div>
                  <ul class="list-inline list-inline-dashed">
                    <li>June 21, 2021 at 8:12pm</li>
                    <li><a class="text-primary" href="https://ld-wt73.template-help.com/wt_prod-20176/blog-masonry.html">News</a></li>
                  </ul>
                  <!-- Post Body-->
                  <div class="post-body">
                    <p>There have been a lot of cases in which people were not provided with accurate reports that eventually affected their medical treatment. There is always...</p>
                  </div>
                  <div class="tags group group-sm">
                  </div>
                </div>
              </article>
            </div>
            <div class="col-md-8 col-lg-4 offset-top-50 offset-md-top-0">
              <!-- Post Modern-->
              <article class="post post-modern post-modern-classic">
                <!-- Post media-->
                <div class="post-media"><a class="link-image" href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html"><img class="img-responsive img-cover" width="370" height="250" src="/images/post-15-370x250.jpg" alt=""></a>
                </div>
                <!-- Post content-->
                <div class="post-content text-left">
                  <!-- Post Title-->
                  <div class="post-title offset-top-8">
                    <h5 class="font-weight-bold"><a href="https://ld-wt73.template-help.com/wt_prod-20176/single-post.html">Preparing for an ECG in 8 Easy Steps: Tips From Our Diagnosticians</a></h5>
                  </div>
                  <ul class="list-inline list-inline-dashed">
                    <li>June 21, 2021 at 8:12pm</li>
                    <li><a class="text-primary" href="https://ld-wt73.template-help.com/wt_prod-20176/blog-masonry.html">News</a></li>
                  </ul>
                  <!-- Post Body-->
                  <div class="post-body">
                    <p>An ECG stands for an "electrocardiogram," which is a test that measures and records the electrical activity of the heart. It is used by doctors to obtain...</p>
                  </div>
                  <div class="tags group group-sm">
                  </div>
                </div>
              </article>
            </div>
          </div>
          <div class="offset-top-50 text-center text-lg-left"><a class="btn btn-ellipse btn-primary" href="https://ld-wt73.template-help.com/wt_prod-20176/blog-grid.html">View all blog posts</a></div>
        </div>
      </section>
      <section class="section">
        <!--Please, add the data attribute data-key="YOUR_API_KEY" in order to insert your own API key for the Google map.-->
        <!--Please note that YOUR_API_KEY should replaced with your key.-->
        <!--Example: <div class="google-map-container" data-key="YOUR_API_KEY">-->
        <div class="google-map-container" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-zoom="5" data-icon="images/gmap_marker.png" data-icon-active="images/gmap_marker_active.png" data-styles="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:60}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:40},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;administrative.province&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:30}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ef8c25&quot;},{&quot;lightness&quot;:40}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b6c54c&quot;},{&quot;lightness&quot;:40},{&quot;saturation&quot;:-40}]},{}]">
          <div class="google-map"></div>
          <ul class="google-map-markers">
            <li data-location="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-description="9870 St Vincent Place, Glasgow"></li>
          </ul>
        </div>
      </section>
      

