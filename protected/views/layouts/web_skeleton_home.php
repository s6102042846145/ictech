<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<script type="text/javascript" async="" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ga.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_02.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script_01.js"></script>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/css.css">
				<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css">
		<!--link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css"-->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-thaisarabun.css">
		<!--<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">-->
    	<style>
		.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
		</style>
  		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dataurl.css">
	
		  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>    

	</head>
	<body>

		<?php echo $content; ?>  

        <!-- jQuery Plugins -->
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/core.min.js"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
	

	</body>

</html>



