<div class="preloader loaded">
  <div class="preloader-body">
    <div class="cssload-container">
      <div class="cssload-double-torus"></div>
    </div>
  </div>
</div>
<header class="section page-header">
  <!--RD Navbar-->
  <div class="rd-navbar-wrap" style="height: 171px;">
    <nav class="rd-navbar rd-navbar-subpage rd-navbar-original rd-navbar-static" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
      <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1 toggle-original" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
      <div class="rd-navbar-aside-outer rd-navbar-collapse toggle-original-elements">
        <!--RD Navbar Brand-->
        <div class="rd-navbar-aside">
          <div class="rd-navbar-brand">
            <!--Brand--><a class="brand" href="/home"><img class="brand-logo-dark" src="/images/logo-default-154x53.png" alt="" width="77" height="26"><img class="brand-logo-light" src="/images/logo-inverse-154x53.png" alt="" width="77" height="26"></a>
          </div>
          <div class="contacts-wrap">
              <div class="contact-info reveal-sm-inline-block text-left offset-none">
                    <div class="p unit unit-spacing-xs unit-horizontal">
                      <div class="unit-left">
                          <span class="icon icon-xs icon-circle icon-gray-light text-primary mdi mdi-account"></span></div>
                      <div class="mt-3 unit-body">
                          <span class="font-italic font-weight-extra-bold">สวัสดีคุณ,</span>
                          <a href="/profile">
                              <?php echo Yii::app()->user->getInfo('fname'); ?> 
                              <?php echo Yii::app()->user->getInfo('lname'); ?>
                          </a>
                        </div>
                    </div>
                  </div>
          
            <div class=" reveal-sm-inline-block text-left" >
              <div class="p unit unit-horizontal unit-spacing-xs">
                <div class="unit-left"><span class="icon icon-xs icon-circle icon-gray-light  mdi mdi-logout"></span></div>
                <div class="mt-3 unit-body"><a href="/logout" class="text-danger">ออกจากระบบ</a></div>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="rd-navbar-main-outer">
        <div class="rd-navbar-main">
          <!--RD Navbar Panel-->
          <div class="rd-navbar-panel">
            <!--RD Navbar Toggle-->
            <button class="rd-navbar-toggle toggle-original" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
            <!--RD Navbar Brand-->
            <div class="rd-navbar-brand">
              <!--Brand--><a class="brand" href="https://ld-wt73.template-help.com/wt_prod-20176/index.html"><img class="brand-logo-dark" src="/images/logo-default-154x53.png" alt="" width="77" height="26"><img class="brand-logo-light" src="/images/logo-inverse-154x53.png" alt="" width="77" height="26"></a>
            </div>
          </div>
          <div class="rd-navbar-main-element">
            <div class="rd-navbar-nav-wrap toggle-original-elements">
              <ul class="rd-navbar-nav">
                   <li class="rd-nav-item">
                    <a class="rd-nav-link" href="/dashboard">หน้าหลัก</a>
                </li>
                <li class="rd-nav-item">
                    <a class="rd-nav-link" href="/dictation">คำสั่ง</a>
                </li>
                
               
                <li class="rd-nav-item">
                    <a class="rd-nav-link" href="/iccstructure">โครงสร้างทีม ICC</a>
                </li>
               
                  <li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu">
                  <a class="rd-nav-link" href="javascript:void(0)">การเฝ้าระวังการติดเชื้อ</a>
                  <span class="d-md-inline d-none mdi mdi-chevron-down rd-navbar-submenu-toggle"></span>
                  <ul class="rd-menu rd-navbar-dropdown dropdown-2">
                    <li class="rd-dropdown-item">
                      <a class="rd-dropdown-link" href="/surveillance">
                        การเฝ้าระวังการติดเชื้อ
                      </a>
                    </li>                    
                    <li class="rd-dropdown-item">
                      <a class="rd-dropdown-link" href="/checklist">
                        แบบเฝ้าระวังการติดเชื้อสำรับตึกผู้ป่วยใน (แบบ Check list)
                      </a>
                    </li>   
                    
                  </ul>
                </li>
                  
                 <li class="rd-nav-item">
                  <a class="rd-nav-link" href="/plan">แผนประจำปี</a>
                </li>
                  <li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu">
                  <a class="rd-nav-link" href="javascript:void(0)">รายงาน</a>
                  <span class="d-md-inline d-none mdi mdi-chevron-down rd-navbar-submenu-toggle"></span>
                  <ul class="rd-menu rd-navbar-dropdown">
                    <li class="rd-dropdown-item">
                      <a class="rd-dropdown-link" href="/report">
                        รายงานการประชุม
                      </a>
                    </li>
                    <li class="rd-dropdown-item">
                      <a class="rd-dropdown-link" href="/reportpaper">
                        แบบรายงาน
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="rd-nav-item">
                  <a class="rd-nav-link" href="/guidelines">แนวทางปฏิบัติ</a>
                </li>
                <li class="rd-nav-item">
                  <a class="rd-nav-link" href="/video">Video</a>
                </li>
                   <li class="rd-nav-item">
                  <a class="rd-nav-link" href="/research">วิจัยและนวัตกรรม</a>
                </li>
                  
              </ul>
            </div>

          </div>
        </div>
      </div>
    </nav>
  </div>
</header>
<!-- Breadcrumbs-->
<section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(../images/background-breadcrumbs-01-1920x345.jpg);" data-preset="{&quot;title&quot;:&quot;Breadcrumbs&quot;,&quot;category&quot;:&quot;header&quot;,&quot;reload&quot;:false,&quot;id&quot;:&quot;breadcrumbs&quot;}">
  <div class="container">
    <h2 class="breadcrumbs-custom-title thsarabunnew">
      <?php

      $str_arr = explode("|", $this->pageTitle);
      echo $str_arr[0];
      ?>
    </h2>
    <ul class="breadcrumbs-custom-path">
      <!--li><a href="#">Home</a></li>
            <li class="active">Login</li-->
    </ul>
  </div>
</section>