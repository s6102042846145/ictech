<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icons/favicon.ico" type="image/x-icon">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo CHtml::encode($this->pageTitle); ?> Administrator</title>  

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/fontawesome-free/css/all.min.css">
  
  
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/fonts/thsarabunnew.css">
     <!-- jQuery -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/jquery/jquery.min.js"></script>
    
    
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-ui/1.10.4/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap/js/bootstrap.min.js"></script>    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/jquery.iframe-transport.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/jquery-upload/js/jquery.fileupload.js"></script>

     <link href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/css/datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
   
    
    
    
    
    
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">
        
</head>
    <body class="hold-transition layout-top-nav">
        <div class="wrapper">
            <div class="content-wrapper">
                <?php echo $content; ?> 
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
              <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
              </div>
          </div>
        </div>
       
        <!-- Bootstrap 4 -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables  & Plugins -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/jszip/jszip.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/pdfmake/pdfmake.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/pdfmake/vfs_fonts.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/datatables-buttons/js/buttons.html5.js"></script>
        
        <!-- AdminLTE App -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/dist/js/demo.js"></script>
        <!-- Page specific script -->
        
        
        <!-- Daterangepicker -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/moment/moment.min.js"></script>
        <!-- date-range-picker -->
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js"></script>
        
        
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- End Daterangepicker -->
        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.min.js"></script> 
        
        <script>
          $(function () {
            $("#example1").DataTable({
              "responsive": true, "lengthChange": false, "autoWidth": false,
              "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
              "paging": true,
              "lengthChange": false,
              "searching": false,
              "ordering": true,
              "info": true,
              "autoWidth": false,
              "responsive": true,
            });
          });
        </script>    
    </body>
</html>


