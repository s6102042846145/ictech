<div class="preloader loaded">
  <div class="preloader-body">
    <div class="cssload-container">
      <div class="cssload-double-torus"></div>
    </div>
  </div>
</div>

<header class="section page-header header-absolute">
        <!--RD Navbar-->
        <div class="rd-navbar-wrap" style="height: 191px;">
          <nav class="rd-navbar rd-navbar-classic rd-navbar-original rd-navbar-static" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1 toggle-original" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
            <div class="rd-navbar-aside-outer rd-navbar-collapse toggle-original-elements">
              <!--RD Navbar Brand-->
              <div class="rd-navbar-aside">
                <div class="rd-navbar-brand">
                  <!--Brand--><a class="brand" href="index.html"><img class="brand-logo-dark" src="images/logo-default-154x53.png" alt="" width="77" height="26"><img class="brand-logo-light" src="images/logo-inverse-154x53.png" alt="" width="77" height="26"></a>
                </div>
                <div class="contacts-wrap">
                  <div class="contacts-wrap">
                  <a class="mr-3 reveal-sm-inline-block text-left offset-none"  href="/register">
                    <div class="p unit unit-spacing-xs unit-horizontal">
                      <div class="unit-left"><span class="icon icon-xs icon-circle icon-white-17 mdi mdi-account"></span></div>
                      <div class="mt-3 unit-body">
						  <span class="text-white">สมัครเข้าใช้งาน</span>
						</div>
                    </div>
                  </a>
                  <a class=" reveal-sm-inline-block text-left" href="/login">
                    <div class="p unit unit-horizontal unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xs icon-circle icon-white-17 mdi mdi-login"></span></div>
                      <div class="mt-3 unit-body"><span class="text-white">เข้าสู่ระบบ</span></div>
                    </div>
                  </a>
                </div>
                </div>
              </div>
            </div>
            <div class="rd-navbar-main-outer">
             <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!--RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!--RD Navbar Toggle-->
                  <button class="rd-navbar-toggle toggle-original" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!--RD Navbar Brand-->
                  <div class="rd-navbar-brand">
                    <!--Brand--><a class="brand" href="https://ld-wt73.template-help.com/wt_prod-20176/index.html"><img class="brand-logo-dark" src="/images/logo-default-154x53.png" alt="" width="77" height="26"><img class="brand-logo-light" src="/images/logo-inverse-154x53.png" alt="" width="77" height="26"></a>
                  </div>
                </div>
                <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap toggle-original-elements">
                    <ul class="rd-navbar-nav">
                      <li class="rd-nav-item active"><a class="rd-nav-link" href="#">Home</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#">About</a>
                      </li>
                      
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#">Services</a>
                      </li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#">Gallery</a>
                      </li>
						<li class="rd-nav-item"><a class="rd-nav-link" href="#">Blog</a>
                      </li>						
                      
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#">Contacts</a>
                      </li>
                    </ul>
                  </div>
                  
                </div>
              </div>
            </div>
            </div>
          </nav>
        </div>
      </header>
      