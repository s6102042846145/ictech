  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white thsarabunnew">
    <div class="container">
      <a href="/admin/dashboard" class="navbar-brand">
        <img src="<?php echo Yii::app()->params['prg_ctrl']['logo'] ?>" alt="<?php echo Yii::app()->name; ?>" class="brand-image" >
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/admin/dashboard" class="nav-link">Home</a>
          </li>
          
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">ตั้งค่าข้อมูล</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">                
                <li class="dropdown-submenu dropdown-hover">
                <a id="drpdepment" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">ข้อมูลตัวชี้วัดของหน่วยงาน</a>
                <ul aria-labelledby="drpdepment" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="/admin/indicators" class="dropdown-item">ข้อมูลตัวชี้วัด</a>
                  </li>

                  <li>
                      <a tabindex="-1" href="/admin/department" class="dropdown-item">ข้อมูลหน่วยงาน</a>
                  </li>
                </ul>
                </li>
                <li><a href="/admin/iccmonitor" class="dropdown-item">ICC Monitor</a></li>
                <li><a href="/admin/problemandsuggestion" class="dropdown-item">ปัญหา/ข้อเสนอแนะ</a></li>                
                <li class="dropdown-divider"></li>
                <li class="dropdown-submenu dropdown-hover">
                <a id="drpsummarize" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">สรุปข้อมูลตัวชี้วัด</a>
                <ul aria-labelledby="drpsummarize" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">รายวัน</a>
                  </li>
                  <li>
                      <a href="#" class="dropdown-item">รายเดือน</a>
                  </li>
                   <li>
                      <a href="#" class="dropdown-item">รายปี</a>
                   </li>
                </ul>
                </li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Uploads</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="/admin/annualplan" class="dropdown-item">แผนประจำปี</a></li>
                <li><a href="/admin/dictation" class="dropdown-item">คำสั่ง</a></li>
                <li><a href="/admin/meetingreport" class="dropdown-item">รายงานการประชุม</a></li>
              

            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownReport" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">รายงานและวิเคราะห์ตัวชี้วัด</a>
            <ul aria-labelledby="drpreport" class="dropdown-menu border-0 shadow">
                
                <li><a href="/admin/hospital-infection" class="dropdown-item">อัตราการติดเชื้อในโรงพยาบาล</a></li>   
                
                <li class="dropdown-submenu dropdown-hover">
                <a id="report" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">การติดเชื้อ CAUTI</a> 
                <ul aria-labelledby="report" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="/admin/hospital-hai" class="dropdown-item">ในรพ. (HAI)</a>
                  </li>
                  <li>
                      <a tabindex="-1" href="/admin/hospital-cai" class="dropdown-item">ในชุมชน (CAI)</a>
                  </li>
                  <li>
                      <a tabindex="-1" href="/admin/hospital-st" class="dropdown-item">จากรายงานของรพ.สต.</a>
                  </li>
                </ul>
                </li>
                
                  <li>
                      <a href="/admin/phlebitis" class="dropdown-item">อัตราการเกิดหลอดเลือดดำอักเสบ (Phlebitis)</a>
                  </li>
                   <li>
                      <a href="/admin/episiotomyinfection" class="dropdown-item">อัตราการเกิดแผลฝีเย็บ (Episiotomy infection)</a>
                   </li>
                   <li>
                      <a href="/admin/omphalitis" class="dropdown-item">อัตราการติดเชื้อที่สะดือของทารกแรกเกิด (Omphalitis)</a>
                   </li>
                   <li class="dropdown-submenu dropdown-hover">
                    <a id="dropdownSubMenu4" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">อัตราการติดเชื้อในทารกแรกเกิด (Neonatal Sepsis) </a>
                    <ul aria-labelledby="dropdownSubMenu4" class="dropdown-menu border-0 shadow">
                      <li><a href="/admin/neonatalsepsis-hospital" class="dropdown-item">ในรพ. (HAI)</a></li>
                      <li><a href="/admin/neonatalsepsis-community" class="dropdown-item">ในชุมชน (CAI)</a></li>
                    </ul>
                  </li>
                  <li>
                      <a href="/admin/lunginfection" class="dropdown-item">อัตราการติดเชื้อที่ปอดในโรงพยาบาล (HAP)</a>
                   </li>
                   <li class="dropdown-submenu dropdown-hover">
                    <a id="dropdownSubMenu5" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">อัตราการติดเชื้อดื้อยา</a>
                    <ul aria-labelledby="dropdownSubMenu5" class="dropdown-menu border-0 shadow">
                      <li><a href="/admin/drugresistant-hospital" class="dropdown-item">ในรพ. (HAI)</a></li>
                      <li><a href="/admin/drugresistant-community" class="dropdown-item">ในชุมชน (CAI)</a></li>
                    </ul>
                  </li>
                  <li class="dropdown-submenu dropdown-hover">
                    <a id="dropdownSubMenu6" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">รายงาน RDU</a>
                    <ul aria-labelledby="dropdownSubMenu6" class="dropdown-menu border-0 shadow">
                        <li><a href="/admin/rdu-ari" class="dropdown-item">ARI</a></li>
                        <li><a href="/admin/rdu-acutediarrhea" class="dropdown-item">Acute diarrhea</a></li>
                        <li><a href="/admin/rdu-freshwound" class="dropdown-item">แผลสด</a></li>
                    </ul>
                  </li>                  
                   <li>
                      <a href="/admin/dental" class="dropdown-item">อัตราการติดเชื้อจากงานทันตกรรม (การผ่าฟันคุด)</a>
                   </li>
                   <li>
                      <a href="/admin/accident" class="dropdown-item">อุบัติการณ์บุคลากรได้รับอุบัติเหตุหรือบาดเจ็บจากการปฏิบัติงาน</a>
                   </li>
                    <li>
                      <a href="/admin/duty" class="dropdown-item">อุบัติการณ์บุคลากรติดเชื้อจากการปฏิบัติหน้าที่</a>
                   </li>
                   <li class="dropdown-submenu dropdown-hover">
                    <a id="dropdownSubMenu7" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">การล้างมือของบุคลากร</a>
                    <ul aria-labelledby="dropdownSubMenu7" class="dropdown-menu border-0 shadow">
                        <li><a href="/admin/stepstowash" class="dropdown-item">การล้างมือถูกต้อง 7 ขั้นตอน </a></li>
                        <li><a href="/admin/moment" class="dropdown-item">ล้างมือตาม 5 moment</a></li>
                    </ul>
                  </li>    
                  <li>
                      <a href="/admin/efficiency" class="dropdown-item">ประสิทธิภาพการทำให้ปราศจากเชื้อของเครื่องมือ </a>
                   </li>
                  <li>
                      <a href="/admin/wastesorting" class="dropdown-item">การปฏิบัติตามระเบียบการคัดแยกขยะถูกต้อง</a>
                   </li>
                  <li>
                      <a href="/admin/waterstandard" class="dropdown-item">อัตราการผ่านค่ามาตรฐานน้ำดื่ม/น้ำใช้</a>
                   </li>
                  <li>
                      <a href="/admin/wastewatertreatment" class="dropdown-item">การผ่านค่ามาตรฐานของระบบบำบัดน้ำเสีย</a>
                   </li>
                  <li>
                      <a href="/admin/ppe" class="dropdown-item">การให้และถอดเครื่องป้องกันร่างกาย (Personal Protective Equipment : PPE)</a>
                   </li>
                  <li>
                      <a href="/admin/annualhealthcheck" class="dropdown-item">การตรวจสุขภาพประจำปีของเจ้าหน้าที่</a>
                   </li>
                  <li>
                      <a href="/admin/vaccination" class="dropdown-item">การฉีดวัคซีนสำหรับเจ้าหน้าที่ ( 1 ครั้ง/ปี)</a>
                   </li>
                    <li>
                      <a href="/admin/covid19atk" class="dropdown-item">การเฝ้าระวังการติดเชื้อ COVID 19 ด้วยการตรวจ ATK</a>
                   </li>
                   <li>
                      <a href="/admin/transmission-base-precautions" class="dropdown-item"> การแยกผู้ป่วยตาม Transmission base precautions </a>
                   </li>
                   <li>
                      <a href="/admin/prevalence-survey" class="dropdown-item"> การสำรวจความชุก ( 1 ครั้ง/ปี) </a>
                   </li>
                   <li>
                      <a href="/admin/surveillance" class="dropdown-item">ประสิทธิภาพการเฝ้าระวังการติดเชื้อ</a>
                   </li>
                </ul>
                
          </li>
          <li class="nav-item">
            <a href="/admin/video" class="nav-link">วิดีโอ</a>
          </li>
          <li class="nav-item">
            <a href="/logout" class="nav-link text-red">Logout</a>
          </li>  
          
          </ul>

      </div>      
    </div>
  </nav>
  <!-- /.navbar -->