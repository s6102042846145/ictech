<?php $this->beginContent('//layouts/web_skeleton_ui'); ?>
    <?php require_once("header.php"); ?>
    <div class="container">
        <?php echo $content; ?>
    </div>
    <?php require_once("footer.php"); ?>
<?php $this->endContent(); ?>