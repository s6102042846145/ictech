<?php
	$this->pageTitle = 'แผนประจำปี' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<div class="pt-3 mt-1 mt-md-0 pt-md-0 pl-3 pr-3 pb-1 pb-md-3 " >
        <div class="card-body p-0">
             <?php 
                function DateThai($strDate)
                {
                    $strYear = date("Y",strtotime($strDate))+543;
                    $strMonth= date("n",strtotime($strDate));
                    $strDay= date("j",strtotime($strDate));
                    $strHour= date("H",strtotime($strDate));
                    $strMinute= date("i",strtotime($strDate));
                    $strSeconds= date("s",strtotime($strDate));
                    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                    $strMonthThai=$strMonthCut[$strMonth];
                    return "$strDay $strMonthThai $strYear";
                }
                //echo "ThaiCreate.Com Time now : ".DateThai($strDate);
            
                $data=lkup_annualplan::getUserSearch();
                foreach($data as $dataitem)
                {
                     $url = Yii::app()->params['prg_ctrl']['url']['upload']."/".Yii::app()->user->getInfo('create_by')."/".$dataitem['file_path'];
                    echo '<div class="row download-quality-doc m-0">
                            <div class="col-12 col-md-9 p-0">
                                <div class="row m-0 p-0">
                                    <div class="col-auto mt-1 p-0" onclick="window.open(\' '. $url .' \');">
                                        <img src="'.Yii::app()->params['prg_ctrl']['pdf'].'" class="float-left  mr-md-3 mr-1 cursor-pointer" >
                                    </div>
                                    <div class="col-auto p-0">
                                        <h6 class="mb-0 cursor-pointer text-bold thsarabunnew" onclick="window.open(\' '. $url .' \');" >
                                           '.$dataitem['name'].'
                                        </h6>
                                        <span>ประกาศ ณ วันที่ '.DateThai($dataitem['file_date']).'</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 pt-2 pt-md-0 text-right p-0">
                                <a href="'.$url.'" download class="ml-1">
                                    <img src="'.Yii::app()->params['prg_ctrl']['download'].'"> ดาวน์โหลด
                                </a>
                            </div>
                        </div><hr>';
                } 
            ?>
            
    </div>
</div>