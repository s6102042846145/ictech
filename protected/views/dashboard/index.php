<?php
	$this->pageTitle = 'Dashboard' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
 <!--COUNTERS-->
      <section class="section-lg section bg-default">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-lg-start">
            <div class="col-md-10 col-lg-12">
              <h6 class="text-lg-left">COUNTERS</h6>
              <hr class="text-subline">
              <div class="row align-items-lg-center text-center row-30">
                <div class="col-md-6 col-lg-3">
                  <!-- Counter type 1-->
                  <div class="counter-type-1">
                    <div class="h3"><span class="counter font-weight-bold animated-first" data-step="1300" data-from="0">15</span>
                      <hr class="divider divider-sm bg-accent">
                    </div>
                    <h5 class="font-weight-bold text-primary offset-top-20">Years of Experience</h5>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3">
                  <!-- Counter type 1-->
                  <div class="counter-type-1">
                    <div class="h3"><span class="counter font-weight-bold animated-first" data-step="2500" data-from="0">3749</span>
                      <hr class="divider divider-sm bg-accent">
                    </div>
                    <h5 class="font-weight-bold text-primary offset-top-20">Satisfied Patients</h5>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3">
                  <!-- Counter type 1-->
                  <div class="counter-type-1">
                    <div class="h3"><span class="counter font-weight-bold animated-first" data-step="1500" data-from="0">56</span>
                      <hr class="divider divider-sm bg-accent">
                    </div>
                    <h5 class="font-weight-bold text-primary offset-top-20">Qualified Doctors</h5>
                  </div>
                </div>
                <div class="col-md-6 col-lg-3">
                  <!-- Counter type 1-->
                  <div class="counter-type-1">
                    <div class="h3"><span class="counter font-weight-bold animated-first" data-step="1300" data-from="0">8</span>
                      <hr class="divider divider-sm bg-accent">
                    </div>
                    <h5 class="font-weight-bold text-primary offset-top-20">Departments</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      