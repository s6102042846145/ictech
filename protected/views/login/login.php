<?php
$this->pageTitle = 'Login' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<!-- Breadcrumbs-->
<section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(../images/background-breadcrumbs-01-1920x345.jpg);">
  <div class="container">
    <h2 class="breadcrumbs-custom-title">Login</h2>
    <ul class="breadcrumbs-custom-path">
      <li>
        <a href="/home">Home</a>
        <i class="mdi mdi-arrow-right pl-3"></i>
      </li>
      <li class="active">Login</li>
    </ul>
  </div>
</section>
<!-- Our Customers-->
<section class="section-lg section bg-default">
  <div class="container">
    <div class="row justify-content-sm-center justify-content-lg-start">
      <div class="col-md-8 col-lg-6 col-xl-5">
        <!-- Responsive-tabs-->
        <div class="responsive-tabs responsive-tabs-classic tabs-custom" data-type="horizontal" style="width: 100%;">
          <ul class="resp-tabs-list tabs-1 text-center tabs-group-default" data-group="tabs-group-default">
            <li class="resp-tab-item tabs-group-default resp-tab-active" role="tab">Login</li>
          </ul>
          <div class="resp-tabs-container text-left tabs-group-default" data-group="tabs-group-default">
            <div class="resp-tab-content tabs-group-default resp-tab-content-active" aria-labelledby="tabs-group-default_tab_item-0" style="display:block">
              <div class="responsive-tabs-padding-none">
                <!-- RD Mailform-->
                <div class="text-left">
                  <div class="form-wrap">
                    <label class="form-label form-label-outside rd-input-label" for="txtusername">Username or e-mail</label>
                    <input class="form-input" id="txtusername" type="text" autocomplete="off">
                  </div>
                  <div class="form-wrap">
                    <label class="form-label form-label-outside rd-input-label" for="txtpassword">Password</label>
                    <input class="form-input" id="txtpassword" type="password" autocomplete="off">
                  </div>
                  <div class="offset-top-20">
                    <div class="d-md-flex offset-none align-items-sm-center text-center text-md-left">
                      <button class="btn btn-ellipse btn-primary" onclick="ajax_auth();" type="button">sign in</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $("#txtpassword").keyup(function(e) {
      if (e.keyCode == 13) {
        ajax_auth();
      }
    });
  });

  function ajax_auth() {

    if (ex_isEmpty($('#txtusername').val())) {
      alert('กรุณาระบุรหัสผู้ใช้');
      return;
    }
    if (ex_isEmpty($('#txtpassword').val())) {
      alert('กรุณาระบุรหัสผ่าน');
      return;
    }

    var data = {
      'YII_CSRF_TOKEN': '<?php echo Yii::app()->request->csrfToken; ?>',
      'txtusername': $('#txtusername').val(),
      'txtpassword': $('#txtpassword').val()
    };
    $.ajax({
      type: 'POST',
      url: '<?php echo Yii::app()->createAbsoluteUrl("login/auth"); ?>',
      data: data,
      dataType: 'json',
      success: success_auth,
      error: error_auth,
    });
  }
  var success_auth = function(data) {
    if (data.status == 'success') {
      location.reload();
    } else if (data.status == 'error') {
      alert(data.msg);
    } else {
      alert('Invalid Exception. a');
    }
  }
  var error_auth = function(data) {
    alert('Invalid Exception. b');
  }
</script>