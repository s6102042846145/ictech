<?php
	$this->pageTitle = 'ข้อมูลสิทธิ์' . Yii::app()->params['prg_ctrl']['pagetitle'];
?>
<section class="section section-20  bg-default text-center section-single">
        <div class="container">
            <div class="block-sm">
               
              <!-- RD Search Form-->
              <div class="form-search rd-search" >
                <div class="form-wrap">
                  <label class="form-label form-search-label form-label rd-input-label text-body" for="search-results">Search...</label>
                  <input class="form-search-input form-input #{inputClass}" id="search-results" type="text"  autocomplete="off">
                </div>
                <button class="form-search-submit" type="submit"><span class="fa fa-search text-primary"></span></button>
              </div>
          
              </div>
            <button class="btn btn-outline-success float-right mb-2 mt-2"><span class="mdi mdi-plus-circle-outline"></span></button>
          <div class="table-custom-responsive">
            <table class="table-custom table-custom-bordered thsarabunnew">
              <thead>
                <tr>
                  <th>Column 1</th>
                  <th>Column 2</th>
                  <th>Column 3</th>
                  <th>Column 4</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Item #1</td>
                  <td>Description</td>
                  <td>Subtotal</td>
                  <td>$3.00</td>
                </tr>
                <tr>
                  <td>Item #2</td>
                  <td>Description</td>
                  <td>Discount</td>
                  <td>$3.00</td>
                </tr>
                <tr>
                  <td>Item #3</td>
                  <td>Description</td>
                  <td>Shipping</td>
                  <td>$3.00</td>
                </tr>
                <tr>
                  <td>Item #4</td>
                  <td>Description</td>
                  <td>Tax</td>
                  <td>$4.00</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>All Items</td>
                  <td>Description</td>
                  <td>Your Total</td>
                  <td>$13.00</td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </section>