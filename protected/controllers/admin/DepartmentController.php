<?php

class DepartmentController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{	
        $this->layout='adm_main';   
		$this->render('index');
	}
    public function actionForm()
	{	
        $id=isset($_GET['id'])?addslashes(trim($_GET['id'])):'';
        $this->layout='adm_main';   
		$this->render('/admin/department/form');
	}
    
    public function actionIndicators()
	{
			$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
			$data = lkup_department::getIndicators($id);
        //echo var_dump($data);exit();
			echo CJSON::encode(array('data'=>$data));
	}
    
    
    public function actionSearch()
	{	
			$data = lkup_department::getSearch();
			echo CJSON::encode(array('data'=>$data));
	}
	
    
    public function actionSavedata()
    {

		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->code=isset($_POST['code'])?addslashes(trim($_POST['code'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionDepartmentdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_department::getDepartment($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'code'=>$data[0]["code"],
			
			));		

	}
    public function actionDeletedata()
    {

		$model=new frm_department;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    public function actionSetdata()
    {

		$model=new frm_department;		
		$model->indicators_id=isset($_POST['indicators_id'])?addslashes(trim($_POST['indicators_id'])):'';	
		$model->department_id=isset($_POST['department_id'])?addslashes(trim($_POST['department_id'])):'';	
		$model->status=isset($_POST['status'])?addslashes(trim($_POST['status'])):'';	
        if($model->save_indicators()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
        
    /*
    public function getData($data) {
		$ret = "<input type='checkbox' value='".$data['id']."' ".$data['checked']." /> ";
		return $ret;
	}	
    */
}