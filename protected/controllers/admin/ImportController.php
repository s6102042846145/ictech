<?php

class ImportController extends Controller
{
	
	public function actionUpload()
	{
		require_once Yii::getPathOfAlias('application') . Yii::app()->params['prg_ctrl']['vendor']['jquery-upload']['path']; 		
		require_once Yii::getPathOfAlias('application') . Yii::app()->params['prg_ctrl']['vendor']['jquery-upload']['path_PDFUploadHandle']; 

		$upload_handler = new ImportUploadHandle(array(
			'accept_file_types' => '/\.(xls|xlsx|txt|jpeg|jpg|png|pdf|doc|docx|zip|rar|7z)$/i',			
			'upload_dir' => Yii::app()->params['prg_ctrl']['path']['upload'].Yii::app()->params['prg_ctrl']['path']['closepath'],	
			'upload_url' => Yii::app()->params['prg_ctrl']['url']['upload'].Yii::app()->params['prg_ctrl']['path']['closepath'],
		));
	
	}
}