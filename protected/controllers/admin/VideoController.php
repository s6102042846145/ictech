<?php

class VideoController extends Controller
{	
	public function actionIndex()
	{		
        $this->layout='adm_main';
		$this->render('index');
	}
    public function actionSearch()
	{	
        $data = lkup_video::getSearch();
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_video::getData($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'detail'=>$data[0]["detail"],
			));		

	}
    public function actionSavedata()
    {
		$model=new frm_video;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
        $model->detail=isset($_POST['detail'])?addslashes(trim($_POST['detail'])):'';        
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');
            }
		}else{
            if($model->save_update()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionDeletedata()
    {

		$model=new frm_video;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    
    
}