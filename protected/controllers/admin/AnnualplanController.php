

<?php

class AnnualplanController extends Controller
{
    
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';
		$this->render('index');
	}
    public function actionSearch()
	{	
        $data = lkup_annualplan::getSearch();
        echo CJSON::encode(array('data'=>$data));
	}
    public function actionGetdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_annualplan::getData($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'filename'=>$data[0]["file_name"],
			'filepath'=>$data[0]["file_path"],
            'postdate'=>$data[0]["file_date"],
			));		

	}
    public function actionSavedata()
    {
		$model=new frm_annualplan;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
		$model->filepath=isset($_POST['filepath'])?addslashes(trim($_POST['filepath'])):'';
		$model->filename=isset($_POST['filename'])?addslashes(trim($_POST['filename'])):'';
        $model->postdate=isset($_POST['postdate'])?addslashes(trim($_POST['postdate'])):'';        
	
		if($model->id==''){
			if($model->save_insert()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }
		}else{
        if($model->save_update()) {
            echo CJSON::encode(array('status' => 'success','msg' => '',));		 
        } else {
            echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                Yii::app()->session->remove('errmsg');	
            }	
        }
    }
    public function actionDeletedata()
    {

		$model=new frm_annualplan;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg'], ));		
                    Yii::app()->session->remove('errmsg');
            }

    }
    
    
}