<?php

class IccmonitorController extends Controller
{
	function init() {
		parent::chkLogin();
	}
	public function actionIndex()
	{		
        $this->layout='adm_main';       
		$this->render('index');
	}
   
    
    public function actionDepartmentdata()
    {
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_department::getDepartment($id);
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'name'=>$data[0]["name"],
			'code'=>$data[0]["code"],
			
			));		

	}
    public function actionDeletedata()
    {

		$model=new frm_departmentsavedata;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';	
        if($model->save_delete()) {
                echo CJSON::encode(array('status' => 'success','msg' => '',));		 
            } else {
                echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_department'], ));		
                    Yii::app()->session->remove('errmsg_department');
            }

    }
}