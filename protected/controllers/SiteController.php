<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
		//$this->redirect('home');	
		if(!Yii::app()->user->isGuest) { 
			if(Yii::app()->user->getInfo('level')==1){
				$this->redirect('/admin/dashboard');
			}else{
				$this->redirect('/dashboard');
			}
			
		} else {
			$this->redirect('/home'); 
		}		
		
	}
	public function actionRGraphBar()
	{
		$this->render('rgraphbar');
	}
	public function actionError()
	{
        
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest){
				echo $error['message'];
            }else{
                //$this->redirect('/error');
				$this->render('error', $error);
            }
		}
	}

}