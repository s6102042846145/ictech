<?php

class UserController extends Controller
{
	
	public function actionIndex()
	{		
		$this->render('index');
	}
	public function actionSearch()
	{
		//if(isset($_GET['ajax']) && isset($_GET['sort'])){
			
		if(isset($_GET['ajax']) && !isset($_POST['YII_CSRF_TOKEN'])){
			$keyword = Yii::app()->session['user_keyword'];	
			$lavel = Yii::app()->session['user_lavel'];		
				
		} else {
			$keyword = isset($_POST['keyword'])?addslashes(trim($_POST['keyword'])):'';	
			$lavel = isset($_POST['lavel'])?addslashes(trim($_POST['lavel'])):'';			
			Yii::app()->session['user_keyword']=$keyword;		
			Yii::app()->session['user_lavel']=$lavel;		
		}
		$model = lkup_user::search($keyword,$lavel);			
		$this->renderPartial('user', array('model'=>$model));
		
		
	}
	public function actionUserdata()
	{
		$id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$data=lkup_user::getUser($id);
		foreach($data as $dataitem){
			
			$id=$dataitem['id'];
			$code=$dataitem['code'];
			$name=$dataitem['name'];
			$address=$dataitem['address'];				
			$tel=$dataitem['tel'];
			$fb=$dataitem['facebook'];
			$line=$dataitem['line'];
			$lavel=$dataitem['lavel'];
			$password=$dataitem['pass'];
			$active=$dataitem['status'];
			
			
		
		}
		echo CJSON::encode(array(
			'status' => 'success',
			'msg' => '',
			'id'=>$id,
			'code'=>$code,
			'name'=>$name,
			'address'=>$address,
			'tel'=>$tel,
			'fb'=>$fb,
			'line'=>$line,
			'lavel'=>$lavel,
			'password'=>$password,
			'active'=>$active,
			
		));		

	}
	
	
	public function actionSavedata()
	{
		
		$model=new frm_user;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		$model->code=isset($_POST['code'])?addslashes(trim($_POST['code'])):'';
		$model->name=isset($_POST['name'])?addslashes(trim($_POST['name'])):'';
		$model->address=isset($_POST['address'])?addslashes(trim($_POST['address'])):'';
		$model->tel=isset($_POST['tel'])?addslashes(trim($_POST['tel'])):'';
		$model->fb=isset($_POST['fb'])?addslashes(trim($_POST['fb'])):'';
		$model->line=isset($_POST['line'])?addslashes(trim($_POST['line'])):'';
		$model->lavel=isset($_POST['lavel'])?addslashes(trim($_POST['lavel'])):'';
		$model->password=isset($_POST['password'])?addslashes(trim($_POST['password'])):'';
		$model->status=isset($_POST['status'])?addslashes(trim($_POST['status'])):'';
		if($model->id==''){
			if($model->save_insert()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
				Yii::app()->session->remove('errmsg_user');
			}
		}else{
			if($model->save_update()) {
				echo CJSON::encode(array('status' => 'success','msg' => '',));		 
			} else {
				echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
				Yii::app()->session->remove('errmsg_user');	
			}	
		}
	}
	public function actionDeletedata(){

		$model=new frm_user;		
		$model->id=isset($_POST['id'])?addslashes(trim($_POST['id'])):'';
		
			if($model->save_delete()) {
					echo CJSON::encode(array('status' => 'success','msg' => '',));		 
				} else {
					echo CJSON::encode(array('status' => 'error','msg' => Yii::app()->session['errmsg_user'], ));		
						Yii::app()->session->remove('errmsg_user');
				}
	}
}