<?php
	
class CustomWebUser extends CWebUser
{
	private $_usermodel;
	private $_bankmodel;
	
	public function getUser()
	{
		$this->_usermodel=lookupdata::getUserlogin(Yii::app()->user->id);
		return true;
	}	
	
	public function getInfo($fieldcode)
	{	
		if($this->_usermodel===null) {$this->getUser();}
		$user = $this->_usermodel;
		if($fieldcode=='id'){ $returnval = trim(stripslashes($user[0]['id'])); }		
		else if($fieldcode=='username')		{ $returnval = $user[0]['username']; }		
		else if($fieldcode=='fname')	{ $returnval = $user[0]['fname']; }
		else if($fieldcode=='lname')	{ $returnval = $user[0]['lname']; }
		else if($fieldcode=='department')	{ $returnval = $user[0]['department']; }
		else if($fieldcode=='division')	{ $returnval = $user[0]['division']; }
		else if($fieldcode=='position')	{ $returnval = $user[0]['position']; }
		else if($fieldcode=='mobile')	{ $returnval = $user[0]['mobile']; }
		else if($fieldcode=='email')	{ $returnval = $user[0]['email']; }
		else if($fieldcode=='address')	{ $returnval = $user[0]['address']; }
		else if($fieldcode=='remark')	{ $returnval = $user[0]['remark']; }
		else if($fieldcode=='img')	{ $returnval = $user[0]['img']; }
		else if($fieldcode=='level')	{ $returnval = $user[0]['level']; }
        else if($fieldcode=='create_by')	{ $returnval = $user[0]['create_by']; }
		else if($fieldcode=='online_status')	{ $returnval = $user[0]['online_status']; }
        else {$returnval='';}			
		return $returnval;
		
	}
	
	public function clearInfo()
	{	
		unset($this->_usermodel);
		return true;
	}
	
}