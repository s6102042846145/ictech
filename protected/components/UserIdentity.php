<?php
	
class UserIdentity extends CUserIdentity
{
	public $username;
	public $password;
	private $_id;
	private $_ad_id;	
	private $_ad_firstname;
	private $_ad_surname;	
	private $_ad_err;
		
	/*
   	const ERROR_NONE=0;
    const ERROR_USERNAME_INVALID=1;
    const ERROR_PASSWORD_INVALID=2;
    const ERROR_UNKNOWN_IDENTITY=100;
	*/		
	const ERROR_USERNAME_NOTADMIN=21;
	const ERROR_USERNAME_NOTLDAP=22;
	const ERROR_USERNAME_ERRLDAP=23;

	public function __construct()
	{
        $arg_list = func_get_args();
		$this->username=$arg_list[0];
		$this->password=$arg_list[1];
	}

	public function authenticate()
	{	
		
		$username=strtolower($this->username);	
		$password=strtolower($this->password);
		$user=mas_user::model()->find('(LOWER(username)="'.$username.'"  and LOWER(password)="'.$password.'" )');
		
       
		if($user===null){ 
		//echo var_dump(2);exit;
			return $this->errorCode=self::ERROR_USERNAME_NOTLDAP;
			//create new user
		} else {
			//echo var_dump($user->status);exit;
			if($user->status=="2"){
				return $this->errorCode=self::ERROR_USERNAME_NOTADMIN;
			}
			$this->_id=$user->id;	
			$this->errorCode=self::ERROR_NONE;
		}

		return $this->errorCode==self::ERROR_NONE;
		
	}	
	
	private function AuthAD($username,$password) {
		$host=Yii::app()->params['prg_ctrl']['ldap']['server']; 
		$port=Yii::app()->params['prg_ctrl']['ldap']['port'];
		$bind_uid=Yii::app()->params['prg_ctrl']['ldap']['bind_uid'];		
		$bind_pwd=Yii::app()->params['prg_ctrl']['ldap']['bind_pwd'];			
		$bind_dn=Yii::app()->params['prg_ctrl']['ldap']['bind_dn'];		
		$filter_attr=Yii::app()->params['prg_ctrl']['ldap']['filter_attr'];		
		$arr_search_attr=Yii::app()->params['prg_ctrl']['ldap']['arr_search_attr'];			
		$arr_basedn=Yii::app()->params['prg_ctrl']['ldap']['arr_basedn'];			
					
		$ldapcon = ldap_connect($host,$port);		
		if(!$ldapcon) { 
			//echo '<br> ldap cannot connect';
			return false; 
		}
		
		ldap_set_option($ldapcon, LDAP_OPT_PROTOCOL_VERSION, 3);
		$ldapbind = ldap_bind($ldapcon,$bind_dn,$bind_pwd);
		if(!$ldapbind) { 
			//echo '<br> ldap_bind Error: '.ldap_error($ldapcon); 
			ldap_close($ldapcon); 
			return false; 
		}
		
		$ldapsr = ldap_search($ldapcon, $arr_basedn, $filter_attr ."=". $username, array_values($arr_search_attr));
		if(!$ldapsr)
		{
			//echo '<br> ldap_search Error: '.ldap_error($ldapcon); 
			ldap_close($ldapcon); 
			return false; 
		}
		
		$entry = @ldap_get_entries($ldapcon, $ldapsr);
		if($entry==false){
			if(!$entry or !$entry[0])
			{
				//echo '<br> ldap_search Error: Not find username'; 
				ldap_close($ldapcon); 
				return false; 			
			}
		}
		
		if (@ldap_bind($ldapcon, $entry[0]['dn'], $password)==true )
		{
			foreach (array_keys($arr_search_attr) as $attr)
			{
				$ldap_user_info[$attr] = $entry[0][$arr_search_attr[$attr]][0];
			}
			$firstname = isset($ldap_user_info['firstname'])?$ldap_user_info['firstname']:'';
			$lastname = isset($ldap_user_info['lastname'])?$ldap_user_info['lastname']:'';
			$mail = isset($ldap_user_info['mail'])?$ldap_user_info['mail']:'';
			
			$this->_ad_firstname=$firstname;
			$this->_ad_surname=$lastname;	
			
			Yii::app()->session['login_name']=$firstname.' '.$lastname;
			ldap_close($ldapcon);
			return true;			
		} else {
			//echo '<br> password incorrect'; 
			ldap_close($ldapcon); 
			return false; 		
		}	
	}
	public function getId()
	{
		return $this->_id;
	}	
}